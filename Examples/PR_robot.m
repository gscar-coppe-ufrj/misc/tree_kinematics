%% This script solves the inverse dynamics problem of a 3DOF leg.
clear all; clc

%% Define joints
offset1 = 0;
offset2 = 0;

joint1 = Joint('joint1','p');
joint2 = Joint('joint2','r');

%% Define rigid bodies (links)
% Link C.M.s
r2 = randn;
% Masses and inertias
Ixx0 = rand; Iyy0 = rand; Izz0 = rand;
Ixx1 = rand; Iyy1 = rand; Izz1 = rand;
Ixx2 = rand; Iyy2 = rand; Izz2 = rand;

m0 = rand; inertia0 = [Ixx0 Iyy0 Izz0 0 0 0]; cm_location0 = [0 0 0];
m1 = rand; inertia1 = [Ixx1 Iyy1 Izz1 0 0 0]; cm_location1 = [0 0 0];
m2 = rand; inertia2 = [Ixx2 Iyy2 Izz2 0 0 0]; cm_location2 = [0 0 r2];

body0 = RigidLink( 'body0', m0, inertia0, cm_location0 );
body1 = RigidLink( 'body1', m1, inertia1, cm_location1 );
body2 = RigidLink( 'body2', m2, inertia2, cm_location2 );

%% Define joint axis and locations
ey = [0 1 0]';
ez = [0 0 1]';

l2 = randn; d = randn;
joint1_location_wrt_body0 = zeros(3,1);
joint1_location_wrt_body1 = zeros(3,1);

joint2_location_wrt_body1 = [0 -d 0]';
joint2_location_wrt_body2 = zeros(3,1);

body0.addJoint(joint1, ez, joint1_location_wrt_body0);
body1.addJoint(joint1, ez, joint1_location_wrt_body1);

body1.addJoint(joint2, -ey, joint2_location_wrt_body1);
body2.addJoint(joint2, -ey, joint2_location_wrt_body2);

%% Define robot
PR = RigidLinkTree('PR');
PR.addLink(body0,'base');
PR.addLink(body1);
PR.addLink(body2);

%% Connect links
PR.connectLinks( 'body0','body1','joint1' );
PR.connectLinks( 'body1','body2','joint2' );

%% Apply gravity force on the C.M.s
g = randn;
% g = 0;
G = g*ez;
PR.gravity(G);

%% Initialize baseLink variables
baseCoord = randn(6,1); V_0b = randn(6,1); dV_0b = randn(6,1);
PR.setBase( baseCoord, V_0b, dV_0b );

%% Initialize joints variables with random values
q = randn(2,1); dq = randn(2,1); ddq = randn(2,1);
PR.setJoints( q, dq, ddq );

q1 = q(1); q2 = q(2);
dq1 = dq(1); dq2 = dq(2);
ddq1 = ddq(1); ddq2 = ddq(2);

%% Compute robot dynamics
% PR.DFS_Algorithm(1,1,0);

% tauq1 = PR.links{1}.joints{1}.tau_q;
% tauq2 = PR.links{2}.joints{2}.tau_q;
% tauq1 = [tauq1;tauq2]

[ tauq1, tau_base1 ] = PR.InverseDynamics();
TAU1 = [ tau_base1 ; tauq1 ]

%% Compare with Euler Lagrange Model
I2 = Iyy2;
   
Mq = [ m1+m2         , -m2*r2*sin(q2) ;...
       -m2*r2*sin(q2) , m2*(r2^2)+I2  ];
Cq = [ 0 , -m2*r2*cos(q2)*dq2 ;...
       0 ,         0          ];
Nq = [ -(m1+m2)*g       ;...
       m2*r2*g*sin(q2) ];

tauq2 = Mq*ddq + Cq*dq + Nq;

error_tauq = tauq1 - tauq2

%% Compute regressor
[ Y, Pi, Pre ] = PR.Regressor;
TAU2 = Y*Pi

TAU_ERROR = TAU1 - TAU2

%% Computse base mass matrix
Mvv = PR.Mvv()

%% Computes cross mass matrix
Mqv = PR.Mqv()

%% Computes joint mass matrix
Mqq = PR.Mqq()
error_Mqq = Mq - Mqq

%% Computes base Coriolis vector
Cv_term = PR.Cv()

%% Computes joint Coriolis vector
Cq_term = PR.Cq()
error_Cq = Cq*dq - Cq_term

%% Computes base gravity vector
Nv = PR.Nv()

%% Computes joint gravity vector
Nq2 = PR.Nq()
error_Nq = Nq - Nq2

%% Computes new tauq
new_tau_base = Mvv*dV_0b + (Mqv')*ddq + Cv_term + Nv
new_tauq = Mqv*dV_0b + Mqq*ddq + Cq_term + Nq2
TAU3 = [ new_tau_base ; new_tauq ]

new_tauq_error = tauq1 - new_tauq
new_tau_base_error = tau_base1 - new_tau_base
NEW_TAU_ERROR = TAU1 - TAU3