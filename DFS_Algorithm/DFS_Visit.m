function [ color, Pre ] = DFS_Visit( tree, u, color, Pre, FW_flag, BP_flag, FORCE_flag, RG_flag )

color(u) = 1; % Grays link u

%% For each other state in the adjacency list:
for v = 1:length( tree.adjacencyList{u} )

    % Compute CM pos, vel and accel of actual body
    if FW_flag == 1
        tree.links{u}.bar2cm();
    end
    
    if color( tree.adjacencyList{u}{v}(1) ) == 0 % If v is yet not discovered
        
        % Computes the forward kinematics of the mechanism.
        if FW_flag == 1
            ForwardStep();
        end
        
        % Upgrade predecessors
        Pre(tree.adjacencyList{u}{v}(1)) = u;
        
        % Go to deeper level (recursion)
        [ color, Pre ] = DFS_Visit( tree, tree.adjacencyList{u}{v}(1) , color, Pre, FW_flag, BP_flag, FORCE_flag, RG_flag );
    end
end

%% End of branch
if BP_flag == 1 && RG_flag == 0
    BackwardStep();
elseif BP_flag == 1 && RG_flag == 1
    BackwardStepRegressor();
end

color(u) = 2; % Blackens vertice u

end