%% This script solves the inverse dynamics problem of a 3DOF leg.
clear all; clc

%% Define joints
joint1 = Joint('joint1','r',1);
joint2 = Joint('joint2','r',2);

%% Define rigid bodies (links)
% Link C.M.s
r1 = randn;
r2 = randn;

% Masses and inertias
Ixx0 = rand; Iyy0 = rand; Izz0 = rand;
Ixx1 = rand; Iyy1 = rand; Izz1 = rand;
Ixx2 = rand; Iyy2 = rand; Izz2 = rand;

m0 = rand; inertia0 = [Ixx0 Iyy0 Izz0 0 0 0]; cm_location0 = [0 0 0];
m1 = rand; inertia1 = [Ixx1 Iyy1 Izz1 0 0 0]; cm_location1 = [0 0 0];
m2 = rand; inertia2 = [Ixx2 Iyy2 Izz2 0 0 0]; cm_location2 = [0 0 0];

body0 = RigidLink( 'body0', m0, inertia0, cm_location0 );
body1 = RigidLink( 'body1', m1, inertia1, cm_location1 );
body2 = RigidLink( 'body2', m2, inertia2, cm_location2 );

%% Define joint axis and locations
ey = [0 1 0]';
ez = [0 0 1]';

l1 = 2*r1;
l2 = 2*r2;

joint1_location_wrt_body0 = [0 0 0];
joint1_location_wrt_body1 = [0 0 -r1];

joint2_location_wrt_body1 = [0 0 r1];
joint2_location_wrt_body2 = [0 0 -r2];

body0.addJoint(joint1, ey, joint1_location_wrt_body0);

body1.addJoint(joint1, ey, joint1_location_wrt_body1);
body1.addJoint(joint2, ey, joint2_location_wrt_body1);

body2.addJoint(joint2, ey, joint2_location_wrt_body2);

%% Define robot
doublependulum = RigidLinkTree('DoublePendulum');
doublependulum.addLink(body0,'base');
doublependulum.addLink(body1);
doublependulum.addLink(body2);

%% Connect links
doublependulum.connectLinks( 'body0','body1','joint1' );
doublependulum.connectLinks( 'body1','body2','joint2' );

%% Initialize baseLink variables
baseCoord = [randn(3,1) ; zeros(3,1)]; V_0b = randn(6,1); dV_0b = randn(6,1);
doublependulum.setBase( 0, baseCoord, V_0b, dV_0b );

%% Initialize joint variables
q = randn(2,1); dq = randn(2,1); ddq = randn(2,1);
% q = randn(2,1); dq = zeros(2,1); ddq = randn(2,1);
doublependulum.setJoints( q, dq, ddq );

%% Apply gravity force on the C.M.s
g = randn;
G = g*ez;
doublependulum.gravity(G);

%% Compute robot dynamics
tic
[ tauq1, tau_base1 ] = doublependulum.InverseDynamics();
time = toc
TAU1 = [ tau_base1 ; tauq1 ]

%% Compare with Euler Lagrange Model
q1 = q(1); q2 = q(2);
dq1 = dq(1); dq2 = dq(2);
ddq1 = ddq(1); ddq2 = ddq(2);

I1 = Iyy1;
I2 = Iyy2;

m11 = m1*(r1^2) + I1 + I2 + m2*(l1^2 + 2*l1*r2*cos(q2)+r2^2);
m12 = m2*(r2^2 + l1*r2*cos(q2)) + I2;
m22 = m2*(r2^2) + I2;
Mq = [ m11 , m12 ; m12 , m22 ]

c11 = -m2*l1*r2*sin(q2)*dq2;
c12 = -m2*l1*r2*sin(q2)*(dq1+dq2);
c21 = m2*l1*r2*sin(q2)*dq1;
c22 = 0;
Cq = [ c11 , c12 ; c21 , c22 ];

n1 = (m1*r1+m2*l1)*g*sin(q1) + m2*g*r2*sin(q1+q2);
n2 = m2*g*r2*sin(q1+q2);
Nq1 = [ n1 ; n2 ]

tauq2 = Mq*ddq + Cq*dq + Nq1;

error_tauq = tauq1 - tauq2

%% Compute regressor
[ Y, Pi ] = doublependulum.Regressor();
TAU2 = Y*Pi

TAU_ERROR = TAU1 - TAU2

%% Compute base mass matrix
Mvv = doublependulum.Mvv();

%% Compute cross ma
Mvq = doublependulum.Mvq();

%% Computes cross mass matrix
Mqv = doublependulum.Mqv()

%% Computes joint mass matrix
Mqq = doublependulum.Mqq()
% error_Mqq = Mq([2,1],[2,1]) - Mqq
% error_Mqq = Mqq1([3,2,1],[3,2,1]) - Mqq;

%% Computes base Coriolis vector
Cv_term = doublependulum.Cv();

%% Computes joint Coriolis vector
Cq_term = doublependulum.Cq()

%% Computes base gravity vector
Nv = doublependulum.Nv();

%% Computes joint gravity vector
Nq2 = doublependulum.Nq()
error_Nq = Nq1 - Nq2

%% Computes new tauq
new_tau_base = Mvv*dV_0b + Mvq*ddq + Cv_term + Nv
new_tauq = Mqv*dV_0b + Mqq*ddq + Cq_term + Nq2
TAU3 = [ new_tau_base ; new_tauq ]

new_error_tau_base = tau_base1 - new_tau_base
new_error_tauq = tauq1 - new_tauq

NEW_TAU_ERROR = TAU1 - TAU3

M = [ Mvv Mvq ; Mqv Mqq ]

%% Now, adds a sensor on the base C.M.
% body0.addSensor( Frame( cm_location0, zeros(3,1) ) );
% doublependulum.setBase( baseCoord, V_0b, dV_0b );
% Mvv = doublependulum.Mvv()

% baseCoord = [randn(3,1) ; randn(3,1)]; V_0b = randn(6,1); dV_0b = randn(6,1);
% doublependulum.setBase( baseCoord, V_0b, dV_0b );
% Mqv = doublependulum.Mqv();
% Cq_term = doublependulum.Cq();
% Nq2 = doublependulum.Nq();
