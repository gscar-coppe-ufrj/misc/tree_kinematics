classdef Frame < handle
%�This class defines a simple coordinate system.
    
    properties
        name
        numPoints
        
        pos = []
        ang = []
        
        coord = []
        twist_vel = []
        twist_accel = []
    end
    
    methods
        
        %% Class contructor
        function obj = Frame( name, pos, ang )
            obj.name = name;
            obj.pos = pos;
            obj.ang = ang;
        end
        
        %% Add point
        function obj = addPoint( obj, point )
            obj.numPoints = obj.numPoints + 1;
            obj.point(obj.numPoints) = point;
        end
        
        %% Set frame coordinates.
        function obj = setFrame( obj, varargin )
            % varargin{1} = coord, varargin{2} = twist_vel, varargin{3} = twist_accel
        
            switch length(varargin)
                case 1
                    obj.coord = varargin{1}(:);
                case 2
                    obj.coord = varargin{1}(:);
                    obj.twist_vel = varargin{2}(:);
                case 3
                    obj.coord = varargin{1}(:);
                    obj.twist_vel = varargin{2}(:);
                    obj.twist_accel = varargin{3}(:);
                otherwise
                    obj.coord = zeros(6,1);
                    obj.twist_vel = zeros(6,1);
                    obj.twist_accel = zeros(6,1);
            end
        end
        
        %% Gets frame coordinates.
        function [ coord, twist_vel, twist_accel ] = getFrame( obj )
            coord = obj.coord;
            twist_vel = obj.twist_vel;
            twist_accel = obj.twist_accel;
        end
        
    end
    
end

