classdef RigidLink < handle
% This class encapsulates properties of a general rigid body (link).
% It is the component class for RigidLinkTree objects.
    
    properties
        name
        mass
        inertia
        
        pos = []
        ang = []
        
        lin_vel = []
        ang_vel = []
        
        lin_accel = []
        ang_accel = []
        
        body_lin_vel = []
        body_ang_vel = []
        
        body_lin_accel = []
        body_ang_accel = []
        
        cm_location = []
        
        cm_pos = []
        cm_linear_vel = []
        cm_linear_accel = []
        
        sensor_frames = {}
        
        numJoints
        joints = {}
        joint_axis = []
        joint_locations = []
        
        external_forces = []
        external_moments = []
        external_force_locations = []
        
        external_local_forces = []
        external_local_moments = []
        external_local_force_locations = []
        
        base_force
        base_torque
        
        Pi = []
        A_tilde
        Pi_tilde
    end
    
    methods
        
        %% Class constructor
        function obj = RigidLink( name, mass, inertia, cm_location )
            obj.name = name;
            obj.numJoints = 0;
            obj.mass = mass;                  % Body mass
            obj.inertia = inertia;            % Body six inertia components [Ixx Iyy Izz Ixy Ixz Iyz], wrt body frame
            obj.cm_location = cm_location(:); % Body C.M. location
        end
        
        %% Adds joint j1 at location (wrt local frame)
        function obj = addJoint(obj, joint, axis, location, varargin)
            obj.numJoints = obj.numJoints + 1;
            obj.joints{obj.numJoints} = joint;
            obj.joint_axis(:,obj.numJoints) = axis(:);
            obj.joint_locations(:,obj.numJoints) = location(:);
            if ~isempty(varargin)
                obj.joints{obj.numJoints}.id = varargin{1};
            end
        end
        
        %% Removes all joints with the same name
        function obj = remJoint(obj, name)
            for i = length(obj.joints)
                if strcmpi(obj.joints{i}.name, name)
                    obj.joints(i) = [];
                end
            end
        end
        
        %% Adds sensor.
        function addSensor( obj, frame )
            num_frames = length(obj.sensor_frames);
            obj.sensor_frames{num_frames+1} = frame;
        end
        
        %% Removes sensor.
        function remSensor( obj, sensor_id )
            obj.sensor_frames(sensor_id) = [];
        end
        
        %% Converts body coord. to inertial coord.
        function [ linear_vel, angular_vel, linear_accel, angular_accel ] = local2inertial( obj )
            R = R_rpy(obj.ang);
            
%             dRPY = T_rpy(obj.ang)*obj.body_ang_vel;
%             dR = dR_rpy(obj.ang, dRPY);
            
            linear_vel = R*obj.body_lin_vel;
%             linear_accel = R*obj.body_lin_accel;
            linear_accel = R*(obj.body_lin_accel + hat(obj.body_ang_vel)*obj.body_lin_vel);
%             linear_accel = dR*obj.body_lin_vel + R*obj.body_lin_accel;
            
            angular_vel = R*obj.body_ang_vel;
            angular_accel = R*obj.body_ang_accel;
%             angular_accel = dR*obj.body_ang_vel + R*obj.body_ang_accel;
            
            obj.lin_vel = linear_vel;
            obj.ang_vel = angular_vel;
            
            obj.lin_accel = linear_accel;
            obj.ang_accel = angular_accel;
        end
        
        %% Converts inertial coord. to body coord.
        function [ body_linear_vel, body_angular_vel, body_linear_accel, body_angular_accel ] = inertial2local( obj )
            R = R_rpy(obj.ang);
            
%             dRPY = T_rpy(obj.ang)*obj.body_ang_vel;
%             dR = dR_rpy(obj.ang, dRPY);
            
            body_linear_vel = (R')*obj.lin_vel;
            body_angular_vel = (R')*obj.ang_vel;

            obj.body_lin_vel = body_linear_vel;
            obj.body_ang_vel = body_angular_vel;
            
%             body_linear_accel = (R')*( obj.lin_accel - dR*obj.body_lin_vel );
%             body_angular_accel = (R')*( obj.ang_accel - dR*obj.body_ang_vel );
            body_linear_accel = (R')*obj.lin_accel - hat(body_angular_vel)*body_linear_vel;
            body_angular_accel = (R')*obj.ang_accel;
            
            obj.body_lin_accel = body_linear_accel;
            obj.body_ang_accel = body_angular_accel;
        end
        
        %% Sets object body coordinates
        function obj = setLocal( obj, varargin )
        % varargin{1} = coord, varargin{2} = twist_vel, varargin{3} = twist_accel
        
            switch length(varargin)
                case 1
                    coord = varargin{1}(:);
                    obj.pos = coord(1:3);
                    obj.ang = coord(4:6);
                case 2
                    coord = varargin{1}(:);
                    twist_vel = varargin{2}(:);
                    obj.pos = coord(1:3);
                    obj.ang = coord(4:6);
                    obj.body_lin_vel = twist_vel(1:3);
                    obj.body_ang_vel = twist_vel(4:6);
                case 3
                    coord = varargin{1}(:);
                    twist_vel = varargin{2}(:);
                    twist_accel = varargin{3}(:);
                    obj.pos = coord(1:3);
                    obj.ang = coord(4:6);
                    obj.body_lin_vel = twist_vel(1:3);
                    obj.body_ang_vel = twist_vel(4:6);
                    obj.body_lin_accel = twist_accel(1:3);
                    obj.body_ang_accel = twist_accel(4:6);
                otherwise
                    obj.pos = zeros(3,1);
                    obj.ang = zeros(3,1);
                    obj.body_lin_vel = zeros(3,1);
                    obj.body_ang_vel = zeros(3,1);
                    obj.body_lin_accel = zeros(3,1);
                    obj.body_ang_accel = zeros(3,1);
            end
            % transforms to inertial too
            obj.local2inertial();
%             obj.bar2cm();
        end
        
        %% Sets object body coordinates
        function obj = setInertial( obj, varargin )
        % varargin{1} = coord, varargin{2} = vel, varargin{3} = accel
        
            switch length(varargin)
                case 1
                    coord = varargin{1}(:);
                    obj.pos = coord(1:3);
                    obj.ang = coord(4:6);
                case 2
                    coord = varargin{1}(:);
                    vel = varargin{2}(:);
                    obj.pos = coord(1:3);
                    obj.ang = coord(4:6);
                    obj.lin_vel = vel(1:3);
                    obj.ang_vel = vel(4:6);
                case 3
                    coord = varargin{1}(:);
                    vel = varargin{2}(:);
                    accel = varargin{3}(:);
                    obj.pos = coord(1:3);
                    obj.ang = coord(4:6);
                    obj.lin_vel = vel(1:3);
                    obj.ang_vel = vel(4:6);
                    obj.lin_accel = accel(1:3);
                    obj.ang_accel = accel(4:6);
                otherwise
                    obj.pos = zeros(3,1);
                    obj.ang = zeros(3,1);
                    obj.lin_vel = zeros(3,1);
                    obj.ang_vel = zeros(3,1);
                    obj.lin_accel = zeros(3,1);
                    obj.ang_accel = zeros(3,1);
            end
            % transforms to local too
            obj.inertial2local();
        end
        
        %% Gets object body coordinates
        function [ coord, twist_vel, twist_accel ] = getLocal( obj )
            coord = [ obj.pos ; obj.ang ];
            twist_vel = [ obj.body_lin_vel ; obj.body_ang_vel ];
            twist_accel = [ obj.body_lin_accel ; obj.body_ang_accel ];
        end
        
        %% Gets object inertial coordinates
        function [ coord, vel, accel ] = getInertial( obj )
            coord = [ obj.pos ; obj.ang ];
            vel = [ obj.lin_vel ; obj.ang_vel ];
            accel = [ obj.lin_accel ; obj.ang_accel ];
        end
        
        %% Converts the local coordinates given by an INS into inertial coordinates, written in Ebar.
        function [ coord, twist_vel, twist_accel ] = sensor2local( obj, sensor_id )
            r_offset = obj.sensor_frames{sensor_id}.pos(:);
            R_offset = R_rpy(obj.sensor_frames{sensor_id}.ang(:));
            g_offset = [ R_offset, r_offset ; 0 0 0 1 ];
            
            p_sensor = obj.sensor_frames{sensor_id}.coord(1:3);
            ang_sensor = obj.sensor_frames{sensor_id}.coord(4:6);
            
            R_ibar = R_rpy(ang_sensor)*(R_offset');
            coord(1:3) = p_sensor - R_ibar*r_offset;
            coord(4:6) = R2rpy( R_ibar );
            
            sensor_twist_vel = [ obj.sensor_frames{sensor_id}.twist_vel(1:3) ; obj.sensor_frames{sensor_id}.twist_vel(4:6) ];
            sensor_twist_accel = [ obj.sensor_frames{sensor_id}.twist_accel(1:3) ; obj.sensor_frames{sensor_id}.twist_accel(4:6) ];
            
            twist_vel = Ad( g_offset )*sensor_twist_vel;
            twist_accel = Ad( g_offset )*sensor_twist_accel;
            
            obj.setLocal( coord, twist_vel, twist_accel );
        end
        
        function [ sensor_coord, sensor_twist_vel, sensor_twist_accel ] = local2sensor( obj, sensor_id )
%             obj.inertial2local();
            r_offset = obj.sensor_frames{sensor_id}.pos(:);
            R_offset = R_rpy(obj.sensor_frames{sensor_id}.ang(:));
            g_offset = [ R_offset, r_offset ; 0 0 0 1 ];
            
            p_local = obj.pos(:);
            ang_local = obj.ang;
            
            R_0b = R_rpy(ang_local);
            sensor_coord(1:3) = p_local + R_0b*r_offset;
            sensor_coord(4:6) = R2rpy( R_0b*R_offset );
            sensor_coord = sensor_coord(:);
            
            local_twist_vel = [ obj.body_lin_vel ; obj.body_ang_vel ];
            local_twist_accel = [ obj.body_lin_accel ; obj.body_ang_accel ];
            
            sensor_twist_vel = invAd( g_offset )*local_twist_vel;
            sensor_twist_accel = invAd( g_offset )*local_twist_accel;
            
            obj.sensor_frames{sensor_id}.setFrame( sensor_coord, sensor_twist_vel, sensor_twist_accel );
        end
        
        %% Gets sensor coordinates.
        function [ coord, twist_vel, twist_accel ] = getSensor( obj, sensor_id )
            if sensor_id == 0
                [ coord, twist_vel, twist_accel ] = obj.getLocal();
            else
                [ coord, twist_vel, twist_accel ] = obj.sensor_frames{sensor_id}.getFrame();
            end
        end
        
        %% Sets sensor coordinates
        function setSensor( obj, sensor_id, varargin )
            if isempty(obj.sensor_frames) || (sensor_id == 0)
                switch length(varargin)
                    case 1
                        obj.setLocal( varargin{1} );
                    case 2
                        obj.setLocal( varargin{1}, varargin{2} );
                    case 3
                        obj.setLocal( varargin{1}, varargin{2}, varargin{3} );
                    otherwise
                        obj.setLocal();
                end
            else
                switch length(varargin)
                    case 1
                        obj.sensor_frames{sensor_id}.setFrame( varargin{1} );
                    case 2
                        obj.sensor_frames{sensor_id}.setFrame( varargin{1}, varargin{2} );
                    case 3
                        obj.sensor_frames{sensor_id}.setFrame( varargin{1}, varargin{2}, varargin{3} );
                    otherwise
                        obj.sensor_frames{sensor_id}.setFrame();
                end
                obj.sensor2local( sensor_id );
            end
        end
            
        %% Converts bar frame pos., vel. and accel. to any point in the body
        function [ point_pos, point_vel, point_accel ] = bar2point( obj, point )
            if ~isempty(obj.pos) && ~isempty(obj.ang) && ~isempty(obj.lin_vel) && ~isempty(obj.lin_accel) && ~isempty(obj.ang_vel) && ~isempty(obj.ang_accel)
                R_ibari = R_rpy( obj.ang(:) );
                inertial_point = R_ibari*point(:);
                point_pos = obj.pos(:) + inertial_point;
                point_vel = obj.lin_vel(:) + hat(obj.ang_vel(:))*inertial_point;
                point_accel = obj.lin_accel(:) + hat(obj.ang_accel(:))*inertial_point + (hat(obj.ang_vel(:))^2)*inertial_point;
            else
                point_pos = [];
                point_vel = [];
                point_accel = [];
            end
        end
        
        %% Converts bar frame pos., vel. and accel. to C.M.
        function [ cm_position, cm_linear_velocity, cm_linear_acceleration ] = bar2cm( obj )
            [ cm_position, cm_linear_velocity, cm_linear_acceleration ] = obj.bar2point( obj.cm_location );
            obj.cm_pos = cm_position;
            obj.cm_linear_vel = cm_linear_velocity;
            obj.cm_linear_accel = cm_linear_acceleration;
        end
        
        %% Apply external force on point (inertial)
        function addForce(obj, force, force_location)
            i = size(obj.external_forces, 2);
            if ( size(force,2) == 1 ) || ( size(force_location,2) == 1 )
                force = force(:);
                force_location = force_location(:);
            end
            obj.external_forces(:,i+1) = force;
            obj.external_force_locations(:,i+1) = force_location;
        end
        
        %% Apply external force on point (local)
        function addLocalForce(obj, force, force_location)
            i = size(obj.external_local_forces, 2);
            if ( size(force,2) == 1 ) || ( size(force_location,2) == 1 )
                force = force(:);
                force_location = force_location(:);
            end
            obj.external_local_forces(:,i+1) = force;
            obj.external_local_force_locations(:,i+1) = force_location;
        end
        
        %% Apply external moments on body (inertial)
        function addMoment(obj, moment)
            i = size(obj.external_moments, 2);
            if ( size(moment,2) == 1 )
                moment = moment(:);
            end
            obj.external_moments(:,i+1) = moment;
        end
        
        %% Apply external moments on body (local)
        function addLocalMoment(obj, moment)
            i = size(obj.external_local_moments, 2);
            if ( size(moment,2) == 1 )
                moment = moment(:);
            end
            obj.external_local_moments(:,i+1) = moment;
        end
        
    end
    
end

%         %% Converts twist variables from sensor frame to bar frame
%         function obj = twist2bar( obj, config, twist_vel, twist_accel, varargin )
%             if isempty(varargin)
%                 if ~isempty(obj.sensor_frames)
%                     
%                 else
%                     
%                 end
%             else
%                 obj.sensor_frames = varargin{1};
%             end
%             
%             obj.pos = 
%             obj.ang = 
%             obj.lin_vel = 
%             obj.ang_vel = 
%             obj.lin_accel = 
%             obj.ang_accel = 
%         end