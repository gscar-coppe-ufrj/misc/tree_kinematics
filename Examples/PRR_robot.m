%% This script solves the inverse dynamics problem of a 3DOF leg.
clear all; clc

%% Define joints
offset1 = 0;
offset2 = 0;
offset3 = 0;

joint1 = Joint('joint1','p');
joint2 = Joint('joint2','r');
joint3 = Joint('joint3','r');

%% Define rigid bodies (links)
% Link C.M.s
r2 = randn;
r3 = randn;
% Masses and inertias
Ixx0 = rand; Iyy0 = rand; Izz0 = rand;
Ixx1 = rand; Iyy1 = rand; Izz1 = rand;
Ixx2 = rand; Iyy2 = rand; Izz2 = rand;
Ixx3 = rand; Iyy3 = rand; Izz3 = rand;

m0 = rand; inertia0 = [Ixx0 Iyy0 Izz0 0 0 0]; cm_location0 = [0 0 0];
m1 = rand; inertia1 = [Ixx1 Iyy1 Izz1 0 0 0]; cm_location1 = [0 0 0];
m2 = rand; inertia2 = [Ixx2 Iyy2 Izz2 0 0 0]; cm_location2 = [0 0 r2];
m3 = rand; inertia3 = [Ixx3 Iyy3 Izz3 0 0 0]; cm_location3 = [0 0 r3];

body0 = RigidLink( 'body0', m0, inertia0, cm_location0 );
body1 = RigidLink( 'body1', m1, inertia1, cm_location1 );
body2 = RigidLink( 'body2', m2, inertia2, cm_location2 );
body3 = RigidLink( 'body3', m3, inertia3, cm_location3 );

%% Define joint axis and locations
ey = [0 1 0]';
ez = [0 0 1]';

l2 = randn; d = randn;
joint1_location_wrt_body0 = zeros(3,1);
joint1_location_wrt_body1 = zeros(3,1);

joint2_location_wrt_body1 = [0 -d 0]';
joint2_location_wrt_body2 = zeros(3,1);

joint3_location_wrt_body2 = [0 0 l2]';
joint3_location_wrt_body3 = zeros(3,1);

body0.addJoint(joint1, ez, joint1_location_wrt_body0);

body1.addJoint(joint1, ez, joint1_location_wrt_body1);
body1.addJoint(joint2, -ey, joint2_location_wrt_body1);

body2.addJoint(joint2, -ey, joint2_location_wrt_body2);
body2.addJoint(joint3, -ey, joint3_location_wrt_body2);

body3.addJoint(joint3, -ey, joint3_location_wrt_body3);

%% Define robot
PRR = RigidLinkTree('PRR');
PRR.addLink(body0,'base');
PRR.addLink(body1);
PRR.addLink(body2);
PRR.addLink(body3);

%% Connect links
PRR.connectLinks( 'body0','body1','joint1' );
PRR.connectLinks( 'body1','body2','joint2' );
PRR.connectLinks( 'body2','body3','joint3' );

%% Apply gravity force on the C.M.s
g = rand;
G = g*ez;
PRR.gravity(G);

%% Initialize base variables
baseCoord = [randn(3,1) ; zeros(3,1)]; 
V_0b = zeros(6,1); dV_0b = zeros(6,1);
PRR.setBase( 0, baseCoord, V_0b, dV_0b );

%% Initialize joints variables with random values
q = zeros(3,1); dq = randn(3,1); ddq = randn(3,1);
PRR.setJoints( q, dq, ddq );

%% Computes robot dynamics
tic
[ tauq1, tau_base1 ] = PRR.InverseDynamics();
time = toc
TAU1 = [ tau_base1 ; tauq1 ]

%% Compare with Euler Lagrange Model
Mass = [m0 m1 m2 m3];
I2 = inertia2;
I3 = inertia3;
L2 = l2;
C2 = r2 - L2;
C3 = r3;

M = MassMatrix( q, Mass, I2, I3, L2, C2, C3 );
C = CoriolisMatrix( q, dq, Mass, L2, C2, C3 );
N = GravityVector( q, Mass, L2, C2, C3, g );

tauq2 = M*ddq + C*dq + N;
error_tauq = tauq1 - tauq2

%% Computes regressor
[ Y, Pi ] = PRR.Regressor;
TAU2 = Y*Pi

TAU_ERROR = TAU1 - TAU2

%% Computse base mass matrix
Mvv = PRR.Mvv()

%% Compute cross ma
Mvq = PRR.Mvq();

%% Computes cross mass matrix
Mqv = PRR.Mqv()

%% Computes joint mass matrix
Mqq = PRR.Mqq()
error_Mqq = M - Mqq

%% Computes base Coriolis vector
Cv_term = PRR.Cv()

%% Computes joint Coriolis vector
Cq_term = PRR.Cq()
error_Cq = C*dq - Cq_term

%% Computes base gravity vector
Nv = PRR.Nv()

%% Computes joint gravity vector
Nq = PRR.Nq()
error_Nq = N - Nq

%% Computes new tauq
new_tau_base = Mvv*dV_0b + Mvq*ddq + Cv_term + Nv
new_tauq = Mqv*dV_0b + Mqq*ddq + Cq_term + Nq
TAU3 = [ new_tau_base ; new_tauq ]

new_tauq_error = tauq1 - new_tauq
new_tau_base_error = tau_base1 - new_tau_base
NEW_TAU_ERROR = TAU1 - TAU3

M = [ Mvv Mvq ; Mqv Mqq ]