function invdAd = invdAd( g, dR, dhatp )
%Ad(g_0b) is the adjoint Ad_g0b 6x6 matrix of a 4x4 homogeneous transformation g0b 

R = g(1:3,1:3);
p = g(1:3,4);

invdAd = zeros(6,6);
invdAd(1:3,1:3) = dR';
invdAd(4:6,4:6) = dR';
invdAd(1:3,4:6) = -dR'*hat(p) - R'*dhatp;

end
