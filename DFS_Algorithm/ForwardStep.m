%% forward kinematics script

%% Gets index of joint wrt to next link
u_joint = tree.adjacencyList{u}{v}(2);

R_actual = R_rpy( tree.links{u}.ang );
k_actual = tree.links{u}.joint_axis(:,u_joint);

p_cm_actual = tree.links{u}.cm_pos;
dp_cm_actual = tree.links{u}.cm_linear_vel;
ddp_cm_actual = tree.links{u}.cm_linear_accel;

w_actual = tree.links{u}.ang_vel;
dw_actual = tree.links{u}.ang_accel;

k = R_actual*k_actual;
r_actual = R_actual*tree.links{u}.cm_location;

tree.q(end+1) = tree.links{u}.joints{u_joint}.q;
tree.dq(end+1) = tree.links{u}.joints{u_joint}.dq;
tree.ddq(end+1) = tree.links{u}.joints{u_joint}.ddq;

%% Propagation of angular vel. and accel. to next body of chain
if strcmpi(tree.links{u}.joints{u_joint}.type, 'r')
    q_rot = tree.links{u}.joints{u_joint}.q;
    dq_rot = tree.links{u}.joints{u_joint}.dq;
    ddq_rot = tree.links{u}.joints{u_joint}.ddq;
else
    q_rot = 0;
    dq_rot = 0;
    ddq_rot = 0;
end

R_next = R_actual*vrrotvec2mat([ k_actual; q_rot ]);
w_next = w_actual + dq_rot*k;
dw_next = dw_actual + ddq_rot*k + dq_rot*hat(w_actual)*k;

tree.links{tree.adjacencyList{u}{v}(1)}.ang = R2rpy( R_next );
tree.links{tree.adjacencyList{u}{v}(1)}.ang_vel = w_next;
tree.links{tree.adjacencyList{u}{v}(1)}.ang_accel = dw_next;

%% Propagation of linear vel. and accel. to next body of chain
if strcmpi(tree.links{u}.joints{u_joint}.type, 'p')
    q_prism = tree.links{u}.joints{u_joint}.q;
    dq_prism = tree.links{u}.joints{u_joint}.dq;
    ddq_prism = tree.links{u}.joints{u_joint}.ddq;
else
    q_prism = 0;
    dq_prism = 0;
    ddq_prism = 0;
end

l_actual = tree.links{u}.joint_locations(:,u_joint);
l = R_actual*l_actual;

v_joint = tree.adjacencyMatrix(tree.adjacencyList{u}{v}(1),u);
l_next = tree.links{tree.adjacencyList{u}{v}(1)}.joint_locations(:,v_joint);
d = R_next*l_next;

disto_next = l + q_prism*k - r_actual;

p_next = p_cm_actual + disto_next - d;
dp_next = dp_cm_actual + hat(w_actual)*(disto_next) - hat(w_next)*d + dq_prism*k;
ddp_next = ddp_cm_actual + hat(dw_actual)*(disto_next) + (hat(w_actual)^2)*(disto_next) - hat(dw_next)*d - (hat(w_next)^2)*d + ddq_prism*k + 2*dq_prism*hat(w_actual)*k;

tree.links{tree.adjacencyList{u}{v}(1)}.pos = p_next;
tree.links{tree.adjacencyList{u}{v}(1)}.lin_vel = dp_next;
tree.links{tree.adjacencyList{u}{v}(1)}.lin_accel = ddp_next;
