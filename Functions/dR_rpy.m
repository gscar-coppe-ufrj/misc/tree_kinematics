function dR_rpy = dR_rpy(eta2,deta2)
%dR_rpy(eta2,deta2) computes the time derivative of the RPY rotation matrix R=Rz(psi)Ry(theta)Rx(phi)

phi=eta2(1);
theta=eta2(2);
psi=eta2(3);
dphi=deta2(1);
dtheta=deta2(2);
dpsi=deta2(3);

dRz_dpsi=[-sin(psi) -cos(psi) 0;cos(psi) -sin(psi) 0;0 0 0];
dRy_dtheta=[-sin(theta) 0 cos(theta);0 0 0;-cos(theta) 0 -sin(theta)];
dRx_dphi=[0 0 0;0 -sin(phi) -cos(phi);0 cos(phi) -sin(phi)];

dR_rpy=dRz_dpsi*Ry(theta)*Rx(phi)*dpsi+Rz(psi)*dRy_dtheta*Rx(phi)*dtheta+Rz(psi)*Ry(theta)*dRx_dphi*dphi;
end