function invTb = invT_rpy(eta2)
%invTb(eta2) is the inverse of the RPY velocity transformation matrix T(eta2),
%eta2=[phi;theta;psi] is the RPY angles

phi=eta2(1);
theta=eta2(2);

invTb=[1 0 -sin(theta);0 cos(phi) cos(theta)*sin(phi);0 -sin(phi) cos(theta)*cos(phi)];

end


