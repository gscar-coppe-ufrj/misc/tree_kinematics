function psiH = psi_fun( H )
% psi operator
psiH = [ zeros(1,3),  H(3,:)   ,  -H(2,:)   ;...
          -H(3,:)  , zeros(1,3),   H(1,:)   ;...
           H(2,:)  , -H(1,:)   , zeros(1,3) ];
end

