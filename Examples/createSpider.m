%% Create spider function

%% Define joints
offset1 = 0;
offset2 = 0;
offset3 = 0;

revolute_joint1 = Joint('revolute_joint1','r',offset1);
revolute_joint2 = Joint('revolute_joint2','r',offset2);
revolute_joint3 = Joint('revolute_joint3','r',offset3);

%% Define rigid bodies (links)
% Link C.M.s
r1 = randn; r2 = randn; r3 = randn;
rx1 = r1*cos(pi/6); ry1 = r1*sin(pi/6);
rx2 = -r2*cos(pi/6); ry2 = r2*sin(pi/6);
rx3 = 0; ry3 = -r3;

% Masses and inertias
Ixx0 = rand; Iyy0 = rand; Izz0 = rand;
Ixx1 = rand; Iyy1 = rand; Izz1 = rand;
Ixx2 = rand; Iyy2 = rand; Izz2 = rand;
Ixx3 = rand; Iyy3 = rand; Izz3 = rand;

m0 = rand; inertia0 = [Ixx0 Iyy0 Izz0 0 0 0]; cm_location0 = [0 0 0];
m1 = rand; inertia1 = [Ixx1 Iyy1 Izz1 0 0 0]; cm_location1 = [rx1 ry1 0];
m2 = rand; inertia2 = [Ixx2 Iyy2 Izz2 0 0 0]; cm_location2 = [rx2 ry2 0];
m3 = rand; inertia3 = [Ixx3 Iyy3 Izz3 0 0 0]; cm_location3 = [rx3 ry3 0];

body0 = RigidLink( 'body0', m0, inertia0, cm_location0 );
body1 = RigidLink( 'body1', m1, inertia1, cm_location1 );
body2 = RigidLink( 'body2', m2, inertia2, cm_location2 );
body3 = RigidLink( 'body3', m3, inertia3, cm_location3 );

%% Define joint axis and locations
ex = [1 0 0];
ey = [0 1 0];
ez = [0 0 1];

l0 = rand; l1 = rand; l2 = rand; l3 = rand;
joint1_location_wrt_body0 = [l0*cos(pi/6),  l0*sin(pi/6), 0];
joint2_location_wrt_body0 = [      0      , -l0*sin(pi/6), 0];
joint3_location_wrt_body0 = [-l0*cos(pi/6),  l0*sin(pi/6), 0];

joint1_location_wrt_body1 = zeros(3,1);
joint2_location_wrt_body2 = zeros(3,1);
joint3_location_wrt_body3 = zeros(3,1);

body0.addJoint(revolute_joint1, ez, joint1_location_wrt_body0);
body0.addJoint(revolute_joint2, ez, joint2_location_wrt_body0);
body0.addJoint(revolute_joint3, ez, joint3_location_wrt_body0);

body1.addJoint(revolute_joint1, ez, joint1_location_wrt_body1);
body2.addJoint(revolute_joint2, ez, joint2_location_wrt_body2);
body3.addJoint(revolute_joint3, ez, joint3_location_wrt_body3);

spider = RigidLinkTree('SpiderBot');
spider.addLink(body0,'base');
spider.addLink(body1);
spider.addLink(body2);
spider.addLink(body3);

%% Connect links
spider.connectLinks( 'body0', 'body1', 'revolute_joint1' );
spider.connectLinks( 'body0', 'body2', 'revolute_joint2' );
spider.connectLinks( 'body0', 'body3', 'revolute_joint3' );

%% Initialize baseLink variables
spider.links{spider.baseLink}.pos = [ 0 0 0 ]';
spider.links{spider.baseLink}.ang = [ 0 0 0 ]';
spider.links{spider.baseLink}.body_lin_vel = [ 0 0 0 ]';
spider.links{spider.baseLink}.body_ang_vel = [ 0 0 0 ]';
spider.links{spider.baseLink}.body_lin_accel = [ 0 0 0 ]';
spider.links{spider.baseLink}.body_ang_accel = [ 0 0 0 ]';

%% Initialize joint variables
spider.HomePosition();

%% Apply gravity force on the C.M.s
g = randn;
G = -ey*g;
spider.gravity(G);