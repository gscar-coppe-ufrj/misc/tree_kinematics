%% This script solves the inverse dynamics problem of a 3DOF leg.
clear all; clc

%% Define joints
offset1 = 0;
joint1 = Joint('joint1','r');

%% Define rigid bodies (links)
% Link C.M.s
r = 0;
% Masses and inertias
% Ixx0 = rand; Iyy0 = rand; Izz0 = rand;
Ixx0 = 1; Iyy0 = 1; Izz0 = 1;
Ixx1 = rand; Iyy1 = rand; Izz1 = rand;

m0 = 1; inertia0 = [Ixx0 Iyy0 Izz0 0 0 0]; cm_location0 = [0 0 0]';
% m1 = rand; inertia1 = [Ixx1 Iyy1 Izz1 0 0 0]; cm_location1 = [0 0 r]';
m1 = 0; inertia1 = zeros(6,1); cm_location1 = [0 0 r]';

body0 = RigidLink( 'body0', m0, inertia0, cm_location0 );
body1 = RigidLink( 'body1', m1, inertia1, cm_location1 );

%% Define joint axis and locations
ey = [0 1 0]';
ez = [0 0 1]';

joint1_location_wrt_body0 = zeros(3,1);
joint1_location_wrt_body1 = zeros(3,1);

body0.addJoint(joint1, ey, joint1_location_wrt_body0);
body1.addJoint(joint1, ey, joint1_location_wrt_body1);

%% Adds sensor
frame_location = [ 1 0 0 ]';
virtual_frame = Frame( 'Virtual frame', frame_location, zeros(3,1) );
body0.addSensor( virtual_frame );

%% Define robot
pendulum = RigidLinkTree('Pendulum');
pendulum.addLink(body0,'base');
pendulum.addLink(body1);

%% Connect links
pendulum.connectLinks( 'body0','body1','joint1' );

%% Apply gravity force on the C.M.s
g = 10;
% g = 0;
G = g*ez;
pendulum.gravity(G);

%% Initialize baseLink variables
sensor_id = 1;
z = 10;
dz = 0;
ddz = 0;

theta_y = 0;
dtheta_y = 2;
ddtheta_y = 2;

baseCoord = [ 0 ; 0 ; z ; 0 ; theta_y ; 0 ]; V_0b = [ 0 ; 0 ; dz ; 0 ; dtheta_y ; 0 ]; dV_0b = [ 0 ; 0 ; ddz ; 0 ; ddtheta_y ; 0 ];
pendulum.setBase( sensor_id, baseCoord, V_0b, dV_0b );

%% Initialize joints variables with random values
q = 0; dq = 0; ddq = 0;
pendulum.setJoints( q, dq, ddq );

%% Compute robot dynamics
[ tauq1, tau_base1 ] = pendulum.InverseDynamics()
TAU1 = [ tau_base1 ; tauq1 ];

%% Compare with Euler Lagrange Model
I = Iyy1;

tauq2 = (m1*(r^2) + I)*ddq + m1*g*r*sin(q);
% p_cm2 = [ r*sin(q) ; 0 ; r*cos(q) ];
% dp_cm2 = [ r*cos(q) ; 0; -r*sin(q) ]*dq;
% ddp_cm2 = [ -r*sin(q) ; 0; -r*cos(q) ]*dq^2 + [ r*cos(q) ; 0; -r*sin(q) ]*ddq;
% 
% error_p_cm = p_cm1 - p_cm2
% error_dp_cm = dp_cm1 - dp_cm2
% error_ddp_cm = ddp_cm1 - ddp_cm2

error_tauq = tauq1 - tauq2;

%% Compute regressor
[ Y, Pi, Pre ] = pendulum.Regressor;
tauq3 = Y(7:end,:)*Pi;
new_error_tauq = tauq3 - tauq1;

%% Compute dynamic matrices
Mvv = pendulum.Mvv();
Mvq = pendulum.Mvq();
Mqv = pendulum.Mqv();
Mqq = pendulum.Mqq();

Cv_term = pendulum.Cv();
Cq_term = pendulum.Cq();

Nv = pendulum.Nv();
Nq = pendulum.Nq();

%% Computes new tauq
% new_tau_base = Mvv*dV_0b + Mvq*ddq + Cv_term + Nv
% new_tauq = Mqv*dV_0b + Mqq*ddq + Cq_term + Nq

M = [ Mvv Mvq ; Mqv Mqq ];