function M = MassMatrix( q, Mass, I2, I3, L2, C2, C3 )
% compute mass matrix for leg prosthesis

m1 = Mass(2);
m2 = Mass(3);
m3 = Mass(4);

% q1 = q(1);
q2 = q(2) + pi/2;
q3 = q(3);

M = zeros(length(q));

M(1,1) = m1+m2+m3;
M(1,2) = m3*( C3*cos(q2+q3)+L2*cos(q2) ) + m2*(C2 + L2)*cos(q2);
M(1,3) = C3*m3*cos(q2+q3);

M(2,1) = M(1,2);
M(2,2) = I2(1,2)+I3(1,2)+(C2^2)*m2+(C3^2)*m3+(L2^2)*(m2+m3)+2*C2*L2*m2+2*C3*L2*m3*cos(q3);
M(2,3) = m3*C3^2+L2*m3*cos(q3)*C3+I3(1,2);

M(3,1) = M(1,3);
M(3,2) = M(2,3);
M(3,3) = m3*C3^2+I3(1,2);

end

