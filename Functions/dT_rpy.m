function dT_rpy = dT_rpy(eta2,deta2)
%dT_rpy(eta2,deta2) computes the time derivative of the RPY velocity transformation matrix
%T_0b(psi,theta,phi)

phi=eta2(1);
theta=eta2(2);
dphi=deta2(1);
dtheta=deta2(2);

a12=cos(phi)*tan(theta)*dphi+sin(phi)/cos(theta)^2*dtheta;
a13=-sin(phi)*tan(theta)*dphi+cos(phi)/cos(theta)^2*dtheta;
a22=-sin(phi)*dphi;
a23=-cos(phi)*dphi;
a32=cos(phi)/cos(theta)*dphi+sin(phi)*sin(theta)/cos(theta)^2*dtheta;
a33=-sin(phi)/cos(theta)*dphi+cos(phi)*sin(theta)/cos(theta)^2*dtheta;

dT_rpy=[0 a12 a13;0 a22 a23;0 a32 a33];

end