function tauq = SpiderInverseDynamics( q, dq, ddq )

global spider

% persistent spider;
% 
% %% Define robot
% if isempty(spider)
%     createSpider();
% end

spider.links{1}.joints{1}.q = q(1);
spider.links{1}.joints{2}.q = q(2);
spider.links{1}.joints{3}.q = q(3);

spider.links{1}.joints{1}.dq = dq(1);
spider.links{1}.joints{2}.dq = dq(2);
spider.links{1}.joints{3}.dq = dq(3);

spider.links{1}.joints{1}.ddq = ddq(1);
spider.links{1}.joints{2}.ddq = ddq(2);
spider.links{1}.joints{3}.ddq = ddq(3);

[ tauq, ~ ] = spider.InverseDynamics();

end

