%% Backpropagation of forces and moments ( Newton-Euler algorithm )

%% Get the array of sucessors of u
sucessors = find(Pre == u);

%% Computes dynamic variables
m_i = tree.links{u}.mass;
inertia = tree.links{u}.inertia;
I_i = [ inertia(1) inertia(4) inertia(5) ;...
        inertia(4) inertia(2) inertia(6) ;...
        inertia(5) inertia(6) inertia(3) ];
R_ibar = R_rpy( tree.links{u}.ang );
r_ibari = R_ibar*tree.links{u}.cm_location;
I_ibar = R_ibar*I_i*(R_ibar');
w_i = tree.links{u}.ang_vel;
dw_i = tree.links{u}.ang_accel;

%% If it is not at the end of the branch yet...
sum_Fci_up = zeros(3,1);
sum_Tci_up = zeros(3,1);
sum_rFci_up = zeros(3,1);
is_end_of_a_branch = (length(tree.adjacencyList{u}) == 1) && (u ~= tree.baseLink);
if ~is_end_of_a_branch
    for i = 1:length(sucessors)
        
        joint_index = tree.adjacencyMatrix( u, sucessors(i) );
        Fci_up = tree.links{u}.joints{joint_index}.f_contact;
        Tci_up = tree.links{u}.joints{joint_index}.tau_contact;
        
        %% Computes upper joint forces/torques
        projection_axis = R_ibar*tree.links{u}.joint_axis(:,joint_index);
        if strcmpi( tree.links{u}.joints{joint_index}.type, 'p' )
            tree.links{u}.joints{joint_index}.tau_q = (Fci_up')*projection_axis;
        elseif strcmpi( tree.links{u}.joints{joint_index}.type, 'r' )
            tree.links{u}.joints{joint_index}.tau_q = (Tci_up')*projection_axis;
        end
        
        %% Sums all contact forces and torques from the upper joints
        l_i = R_ibar*tree.links{u}.joint_locations(:,joint_index);
        if strcmpi(tree.links{u}.joints{joint_index}.type, 'p')
            q_i = tree.links{u}.joints{joint_index}.q;
        else
            q_i = 0;
        end
        k_i = q_i*R_ibar*tree.links{u}.joint_axis(:,joint_index);
        rci_up = l_i + k_i - r_ibari;
        
        sum_Fci_up = sum_Fci_up + Fci_up;
        sum_Tci_up = sum_Tci_up + Tci_up;
        sum_rFci_up = sum_rFci_up + hat( rci_up )*Fci_up;
    end
end

%% Is there gravity?
if ~isempty(tree.G) && FORCE_flag
    G = tree.G;
else
    G = zeros(3,1);
end

%% Sum all other external forces besides gravity
sum_Fext = zeros(3,1);
sum_rFext = zeros(3,1);
sum_Fext_local = zeros(3,1);
sum_rFext_local = zeros(3,1);
sum_Text = zeros(3,1);
sum_Text_local = zeros(3,1);
if FORCE_flag
    %% Sum up all external forces (inertial)
    for i = 1:size(tree.links{u}.external_forces, 2)
        Fext = tree.links{u}.external_forces(:,i);
        r_ext = R_ibar*tree.links{u}.external_force_locations(:,i);
        
        sum_Fext = sum_Fext + Fext;
        sum_rFext = sum_rFext + hat(r_ext - r_ibari)*Fext;
    end
    
    %% Sum up all local external forces
    for i = 1:size(tree.links{u}.external_local_forces, 2)
        Fext_local = R_ibar*tree.links{u}.external_local_forces(:,i);
        r_ext = R_ibar*tree.links{u}.external_local_force_locations(:,i);
        
        sum_Fext_local = sum_Fext_local + Fext_local;
        sum_rFext_local = sum_rFext_local + hat(r_ext - r_ibari)*Fext_local;
    end
    
    %% Sum up all external moments (inertial)
    for i = 1:size(tree.links{u}.external_moments, 2)
        Text = tree.links{u}.external_moments(:,i);
        sum_Text = sum_Text + Text;
    end
    
    %% Sum up all local external moments
    for i = 1:size(tree.links{u}.external_local_moments, 2)
        Text_local = R_ibar*tree.links{u}.external_local_moments(:,i);
        sum_Text_local = sum_Text_local + Text_local;
    end
end

if u ~= tree.baseLink
    down_joint_index = tree.adjacencyMatrix(u,Pre(u));
    l_u = R_ibar*tree.links{u}.joint_locations(:,down_joint_index);
else
    l_u = zeros(3,1);
end
rci_down = l_u - r_ibari;

ddp_i = tree.links{u}.cm_linear_accel;
Fci = m_i*(ddp_i - G) + sum_Fci_up - sum_Fext - sum_Fext_local;
Tci = I_ibar*dw_i + hat(w_i)*I_ibar*w_i + sum_rFci_up + sum_Tci_up - hat(rci_down)*Fci - sum_Text - sum_Text_local - sum_rFext - sum_rFext_local;

%% Computes joint torques
if u == tree.baseLink
%     tree.links{u}.base_force = Fci;
%     tree.links{u}.base_torque = Tci;
    tree.links{u}.base_force = (R_ibar')*Fci;
    tree.links{u}.base_torque = (R_ibar')*Tci;
else    
    contact_joint_index = tree.adjacencyMatrix(Pre(u),u);
    tree.links{Pre(u)}.joints{contact_joint_index}.f_contact = Fci;
    tree.links{Pre(u)}.joints{contact_joint_index}.tau_contact = Tci;
end
