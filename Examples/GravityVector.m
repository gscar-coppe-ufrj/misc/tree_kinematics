function G = GravityVector( q, Mass, L2, C2, C3, g )

m1 = Mass(2);
m2 = Mass(3);
m3 = Mass(4);

% q1 = q(1);
q2 = q(2) + pi/2;
q3 = q(3);

G = [ -g*(m1+m2+m3); ...
     -C3*g*m3*cos(q2+q3)-g*(m2*(C2+L2)+L2*m3)*cos(q2); ...
     -C3*g*m3*cos(q2+q3)];

end