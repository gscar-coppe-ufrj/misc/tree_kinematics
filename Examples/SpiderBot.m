%% Test script for RigidLinkTree class.
clear all; clc

%% Define joints
offset1 = 0;
offset2 = 0;
offset3 = 0;

revolute_joint1 = Joint('revolute_joint1','r');
revolute_joint2 = Joint('revolute_joint2','r');
revolute_joint3 = Joint('revolute_joint3','r');

%% Define rigid bodies (links)
% Link C.M.s
r1 = randn; r2 = randn; r3 = randn;
rx1 = r1*cos(pi/6); ry1 = r1*sin(pi/6);
rx2 = -r2*cos(pi/6); ry2 = r2*sin(pi/6);
rx3 = 0; ry3 = -r3;

% Masses and inertias
Ixx0 = rand; Iyy0 = rand; Izz0 = rand;
Ixx1 = rand; Iyy1 = rand; Izz1 = rand;
Ixx2 = rand; Iyy2 = rand; Izz2 = rand;
Ixx3 = rand; Iyy3 = rand; Izz3 = rand;

m0 = rand; inertia0 = [Ixx0 Iyy0 Izz0 0 0 0]; cm_location0 = [0 0 0];
m1 = rand; inertia1 = [Ixx1 Iyy1 Izz1 0 0 0]; cm_location1 = [rx1 ry1 0];
m2 = rand; inertia2 = [Ixx2 Iyy2 Izz2 0 0 0]; cm_location2 = [rx2 ry2 0];
m3 = rand; inertia3 = [Ixx3 Iyy3 Izz3 0 0 0]; cm_location3 = [rx3 ry3 0];

body0 = RigidLink( 'body0', m0, inertia0, cm_location0 );
body1 = RigidLink( 'body1', m1, inertia1, cm_location1 );
body2 = RigidLink( 'body2', m2, inertia2, cm_location2 );
body3 = RigidLink( 'body3', m3, inertia3, cm_location3 );

%% Define joint axis and locations
ex = [1 0 0];
ey = [0 1 0];
ez = [0 0 1];

l0 = rand; l1 = rand; l2 = rand; l3 = rand;
joint1_location_wrt_body0 = [l0*cos(pi/6),  l0*sin(pi/6), 0];
joint2_location_wrt_body0 = [      0      , -l0*sin(pi/6), 0];
joint3_location_wrt_body0 = [-l0*cos(pi/6),  l0*sin(pi/6), 0];

joint1_location_wrt_body1 = zeros(3,1);
joint2_location_wrt_body2 = zeros(3,1);
joint3_location_wrt_body3 = zeros(3,1);

body0.addJoint(revolute_joint1, ez, joint1_location_wrt_body0);
body0.addJoint(revolute_joint2, ez, joint2_location_wrt_body0);
body0.addJoint(revolute_joint3, ez, joint3_location_wrt_body0);

body1.addJoint(revolute_joint1, ez, joint1_location_wrt_body1);
body2.addJoint(revolute_joint2, ez, joint2_location_wrt_body2);
body3.addJoint(revolute_joint3, ez, joint3_location_wrt_body3);

%% Define robot
% global spider

spider = RigidLinkTree('SpiderBot');
spider.addLink(body0,'base');
spider.addLink(body1);
spider.addLink(body2);
spider.addLink(body3);

%% Connect links
spider.connectLinks( 'body0', 'body1', 'revolute_joint1' );
spider.connectLinks( 'body0', 'body2', 'revolute_joint2' );
spider.connectLinks( 'body0', 'body3', 'revolute_joint3' );

%% Apply gravity force on the C.M.s
g = randn;
% g = 0;
G = -ey*g;
spider.gravity(G);

%% Initialize baseLink variables
baseCoord = [randn(3,1) ; randn(3,1)]; V_0b = randn(6,1); dV_0b = randn(6,1);
spider.setBase( 0, baseCoord, V_0b, dV_0b );

%% Initialize joint variables
q = randn(3,1); dq = randn(3,1); ddq = randn(3,1);
spider.setJoints( q, dq, ddq );

q1 = q(1); q2 = q(2); q3 = q(3);

%% Compute robot dynamics
[ tauq1, tau_base1 ] = spider.InverseDynamics();
TAU1 = [ tau_base1 ; tauq1 ]

%% Compare with Euler Lagrange model
I1 = Izz1;
I2 = Izz2;
I3 = Izz3;

phi0 = 0;
phi1 = pi/6;
phi2 = 5*pi/6;
phi3 = -pi/2;

Mqq1 = blkdiag( (m1*r1^2 + I1), (m2*r2^2 + I2), (m3*r3^2 + I3) )
Nq1 = [ m1*r1*g*cos(phi0 + phi1 + q1) ;...
        m2*r2*g*cos(phi0 + phi2 + q2) ;...
        m3*r3*g*cos(phi0 + phi3 + q3) ];
Mddq = Mqq1*ddq;
tauq2 = Mddq + Nq1;
error_tauq = tauq1 - tauq2;

%% Compute regressor
[ Y, Pi ] = spider.Regressor();
TAU2 = Y*Pi

TAU_ERROR = TAU1 - TAU2

%% Compute mass matrices
Mvv = spider.Mvv()

Mvq = spider.Mvq();
Mqv = spider.Mqv();
error_cross = Mqv' - Mvq

Mqq = spider.Mqq();
error_Mqq = Mqq1 - Mqq

%% Computes Coriolis terms
Cv_term = spider.Cv();
Cq_term = spider.Cq();

%% Computes gravity vectors
Nv = spider.Nv();
Nq = spider.Nq();
error_Nq = Nq1 - Nq

%% Computes new tauq
new_tau_base = Mvv*dV_0b + Mvq*ddq + Cv_term + Nv;
new_tauq = Mqv*dV_0b + Mqq*ddq + Cq_term + Nq;
TAU3 = [ new_tau_base ; new_tauq ];

new_error_tau_base = tau_base1 - new_tau_base
new_error_tauq = tauq1 - new_tauq
NEW_TAU_ERROR = TAU1 - TAU3;

M = [ Mvv Mvq ; Mqv Mqq ];
eig(M)

%% Compute direct dynamics
ddq1 = ddq
dV_0b1 = dV_0b
[ ddq2, dV_0b2 ] = spider.DirectDynamics(tauq1, tau_base1)

%% Now, adds a sensor on the base C.M.
% body0.addSensor( Frame( cm_location0+[1 1 1], zeros(3,1) ) );
% spider.setBase( baseCoord, V_0b, dV_0b );
% Mvv1 = Mvv
% Mvv2 = spider.Mvv()
% 
% Mvq = spider.Mvq();
% Mqv = spider.Mqv();
