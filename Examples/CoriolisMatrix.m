function C = CoriolisMatrix( q, dq, Mass, L2, C2, C3 )
% compite Coriolis matrix for leg prosthesis

m2 = Mass(3);
m3 = Mass(4);

% q1 = q(1);
q2 = q(2) + pi/2;
q3 = q(3);

% dq1 = dq(1);
dq2 = dq(2);
dq3 = dq(3);

C = zeros(length(q));

C(1,1) = 0;
C(1,2) = -dq2*(L2*m3+m2*(C2+L2))*sin(q2)-C3*m3*(dq2+dq3)*sin(q2+q3);
C(1,3) = -C3*m3*sin(q2+q3)*(dq2+dq3);

C(2,1) = 0;
C(2,2) = -C3*L2*m3*dq3*sin(q3);
C(2,3) = -C3*L2*m3*sin(q3)*(dq2+dq3);

C(3,1) = 0;
C(3,2) = C3*L2*m3*dq2*sin(q3);
C(3,3) = 0;

end

