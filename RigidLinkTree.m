classdef RigidLinkTree < handle
% This class encapsulates the kinematics and dynamics of a general tree of
% interconnected rigid bodies (links). The tree kinematic structure is
% similar to a non-oriented, acyclic graph.

    properties
        name
        numLinks = 0
        numJoints = 0
        baseLink
        baseFrame
        links = {}
        joints = {}
        joint_id = []
        adjacencyList = {}
        adjacencyMatrix = []
        
        gen_coordinates = []
        gen_velocities = []
        gen_accelerations = []
        
        G = []
        
        q = []
        dq = []
        ddq = []
        
        tau_q = []
        tau_base = []
        
        Pi = []
        Y = []
    end
    
    methods
        
        %% Class constructor
        function obj = RigidLinkTree(name)
            obj.name = name;
        end
        
        %% Adds link to link tree.
        function obj = addLink(obj, link, varargin)
            obj.numLinks = obj.numLinks + 1;
            obj.links{obj.numLinks} = link;
            obj.adjacencyList{obj.numLinks} = {};
            obj.adjacencyMatrix(obj.numLinks,obj.numLinks) = 0;
            
            if (~isempty(varargin)) && strcmpi(varargin{1},'base') 
                obj.baseLink = obj.numLinks;
            end
            
            % Adds body joint references to tree object
            for i = 1:length(obj.links{obj.numLinks}.joints)
                is_new_joint = 1;
                for j = 1:length(obj.joints)
                    if strcmpi(obj.links{obj.numLinks}.joints{i}.name,obj.joints{j}.name)
                        is_new_joint = 0;
                        break
                    end
                end
                if is_new_joint || isempty(obj.joints)
                    obj.joints{end+1} = obj.links{obj.numLinks}.joints{i};
                end
            end
        end
        
        %% Removes all links with the same name.
        function obj = remLink(obj, name)
            for i = length(obj.links)
                if strcmpi(obj.links{i}.name, name)
                    obj.links(i) = [];
                end
            end
        end
        
        %% Defines base link of the tree.
        function obj = defBaseLink(obj, name)
            for link_index = 1:length(obj.links)
                if strcmpi(obj.links{link_index}.name, name)
                    obj.baseLink = link_index;
                end
            end
        end
        
        %% Adds connection between two bodies sharing the same joint
        function obj = connectLinks( obj, varargin )
            
            if mod(length(varargin),3) ~= 0
                display('List sizes are not consistent.');
                return
            else
                numConnections = length(varargin)/3;
                
                linkNames1 = varargin((0*numConnections+1):(1*numConnections));
                linkNames2 = varargin((1*numConnections+1):(2*numConnections));
                jointNames = varargin((2*numConnections+1):(3*numConnections));
                
                % Main loop (one for each connection)
                for i = 1:length(jointNames)
                    obj.numJoints = obj.numJoints + 1;
                    % Get index of link 1
                    for link1_index = 1:length(obj.links)
                        if strcmpi(obj.links{link1_index}.name,linkNames1{i})
                            break
                        end
                    end
                    % Get corresponding joint of link1
                    for jointPairIndex1 = 1:length(obj.links{link1_index}.joints)
                        if strcmpi( jointNames{i}, obj.links{link1_index}.joints{jointPairIndex1}.name )
                            break
                        end
                    end
                    
                    % Get index of link 2
                    for link2_index = 1:length(obj.links)
                        if strcmpi(obj.links{link2_index}.name,linkNames2{i})
                            break
                        end
                    end
                    % Get corresponding joint of link2
                    for jointPairIndex2 = 1:length(obj.links{link2_index}.joints)
                        if strcmpi( jointNames{i}, obj.links{link2_index}.joints{jointPairIndex2}.name )
                            break
                        end
                    end
                    
                    % Connect links 'link1_index' and 'link2_index' by
                    % updating adjacencyMatrix and adjacencyLists
                    obj.adjacencyMatrix(link1_index,link2_index) = jointPairIndex1;
                    obj.adjacencyMatrix(link2_index,link1_index) = jointPairIndex2;
                    
                    obj.adjacencyList{link1_index}{length(obj.adjacencyList{link1_index})+1} = [ link2_index jointPairIndex1 ];
                    obj.adjacencyList{link2_index}{length(obj.adjacencyList{link2_index})+1} = [ link1_index jointPairIndex2 ];
                end
            end
        end
        
        %% Gets joint variables
        function [ q, dq, ddq ] = getJoints( obj )
            q = obj.q;
            dq = obj.dq;
            ddq = obj.ddq;
        end
        
        %% Sets joint variables (coord., vel. and accel.)
        function setJoints(obj, varargin)
            
            switch length(varargin)
                case 1
                    q_in = varargin{1}(:);
                case 2
                    q_in = varargin{1}(:);
                    dq_in = varargin{2}(:);
                case 3
                    q_in = varargin{1}(:);
                    dq_in = varargin{2}(:);
                    ddq_in = varargin{3}(:);
                otherwise
                    q_in = zeros(obj.numJoints,1);
                    dq_in = zeros(obj.numJoints,1);
                    ddq_in = zeros(obj.numJoints,1);
            end
            
            obj.q = q_in;
            obj.dq = dq_in;
            obj.ddq = ddq_in;
            
            for i = 1:obj.numJoints
%                 obj.joints{i}.q = q_in(obj.joints{i}.id);
%                 obj.joints{i}.dq = dq_in(obj.joints{i}.id);
%                 obj.joints{i}.ddq = ddq_in(obj.joints{i}.id);

                obj.joints{i}.q = q_in(i);
                obj.joints{i}.dq = dq_in(i);
                obj.joints{i}.ddq = ddq_in(i);
            end
        end

        %% Gets base variables.
        function [ coord, twist_vel, twist_accel ] = getBase( obj, varargin )
        % If a sensor is defined, then this function gets the coordinates
        % of the sensor frame instead.
        
            if isempty(obj.links{obj.baseLink}.sensor_frames) || isempty(varargin)
                [ coord, twist_vel, twist_accel ] = obj.links{obj.baseLink}.getLocal();
            else
                sensor_id = varargin{1};
                [ coord, twist_vel, twist_accel ] = obj.links{obj.baseLink}.getSensor( sensor_id );
            end
        end
        
        %% Sets base variables (coord., vel., accel)
        function setBase(obj, sensor_id, varargin)
        % varargin{1} = coord, varargin{2} = twist_vel, varargin{3} = twist_accel
        % If a sensor is defined, then this function sets the coordinates
        % of the sensor frame instead, according to sensor_id = varargin{1}.

            if isempty(obj.links{obj.baseLink}.sensor_frames) || (sensor_id == 0)
                switch length(varargin)
                    case 1
                        obj.links{obj.baseLink}.setLocal( varargin{1} );
                    case 2
                        obj.links{obj.baseLink}.setLocal( varargin{1}, varargin{2} );
                    case 3
                        obj.links{obj.baseLink}.setLocal( varargin{1}, varargin{2}, varargin{3} );
                    otherwise
                        obj.links{obj.baseLink}.setLocal();
                end
            else
                switch length(varargin)
                    case 1
                        obj.links{obj.baseLink}.setSensor( sensor_id, varargin{1} );
                    case 2
                        obj.links{obj.baseLink}.setSensor( sensor_id, varargin{1}, varargin{2} );
                    case 3
                        obj.links{obj.baseLink}.setSensor( sensor_id, varargin{1}, varargin{2}, varargin{3} );
                    otherwise
                        obj.links{obj.baseLink}.setSensor( sensor_id );
                end
            end
        end
        
        %% Initializes all tree variables with zeros.
        function obj = HomePosition( obj, varargin )
            
            if isempty(varargin)
                obj.setBase(0);
            else
                sensor_id = varargin{1};
                obj.setBase( sensor_id );
            end
            
            obj.q = zeros(obj.numJoints,1);
            obj.dq = zeros(obj.numJoints,1);
            obj.ddq = zeros(obj.numJoints,1);
            
            for i = 1:obj.numJoints
                obj.joints{i}.q = 0;
                obj.joints{i}.dq = 0;
                obj.joints{i}.ddq = 0;
            end
        end
        
        %% Clears tree (all link kinematic variables become undefined - but baseLink)
        function clearTree( obj )
            obj.q = [];
            obj.dq = [];
            obj.ddq = [];
            
            for i = 1:obj.numLinks
                if i ~= obj.baseLink
                    obj.links{i}.pos = [];
                    obj.links{i}.ang = [];
                    
                    obj.links{i}.lin_vel = [];
                    obj.links{i}.ang_vel = [];
                    
                    obj.links{i}.lin_accel = [];
                    obj.links{i}.ang_accel = [];
                    
                    obj.links{i}.cm_pos = [];
                    obj.links{i}.cm_linear_vel = [];
                    obj.links{i}.cm_linear_accel = [];
                end
            end
        end
                
        %% Applies external wrench on body named 'body_name'.
        function obj = extWrench(obj, body_name, force, force_location, moment)
            for body_index = 1:obj.numLinks
                if strcmpi( obj.links{body_index}.name, body_name )
                    break
                end
            end
            obj.links{body_index}.extForce(force, force_location);
            obj.links{body_index}.extMoment(moment);
        end

        %% Applies gravity force to all bodies
        function obj = gravity(obj, G)
            obj.G = G(:);
        end
        
        %% This function performs a depth-first search in the whole kinematic tree.
        function Pre = DFS_Algorithm( obj, FW_flag, BP_flag, FORCE_flag, RG_flag, varargin )
        % The flags FW_flag and BP_flag control the computation of the
        % forward kinematics/ backwards dynamics of each link. FORCE_flag 
        % controls if external forces (including gravity) are taken into 
        % consideration or not. RG_flag controls the regressor matrix computation. 
        % It returns the vector of predecessors of each rigid link (Pi).
            
            %% Initialization of kinematic variables for all links
            for i = 1:obj.numJoints
                if isempty(obj.joints{i}.q) || isempty(obj.joints{i}.dq) || isempty(obj.joints{i}.ddq)
                    display('Undefined joint variables.');
                    return
                end
            end
            
            %% Clear variables.
            obj.clearTree();
            
            %% If link body variables where given, define base link body variables.
%             if ~isempty(varargin)
%                 obj.setBase( varargin{1}, varargin{2}, varargin{3} ); 
%             else
                bool1 = isempty( obj.links{obj.baseLink}.pos ) || isempty( obj.links{obj.baseLink}.ang );
                bool2 = isempty( obj.links{obj.baseLink}.body_lin_vel ) || isempty( obj.links{obj.baseLink}.body_ang_vel );
                bool3 = isempty( obj.links{obj.baseLink}.body_lin_accel ) || isempty( obj.links{obj.baseLink}.body_ang_accel );
                if bool1 || bool2 || bool3
                    display('Undefined base variables.');
                    return
                end
%             end
            
            %% Execute depth first search algorithm
            [ ~, Pre ] = DFS( obj, FW_flag, BP_flag, FORCE_flag, RG_flag );
        end
        
        % This function computes the forward kinematics algorithm for the whole tree.
        function [ Pre ] = ForwardKinematics( obj )
            Pre = obj.DFS_Algorithm( 1, 0, 0, 0 );
        end
        
        %% This function computes the inverse dynamics algorithm of the whole tree.
        function [ tau_q, base_wrench ] = InverseDynamics( obj, varargin )
        % 'intrinsic' ignores the presence of all external forces and moments 
        % (including gravity)
            if isempty(varargin)
                obj.DFS_Algorithm( 1, 1, 1, 0 );
            else
                if strcmpi(varargin{2},'intrinsic')
                    obj.DFS_Algorithm( 1, 1, 0, 0 );
                elseif strcmpi(varargin{2},'extrinsic')
                    obj.DFS_Algorithm( 1, 1, 1, 0 );
                end
            end
            
            % Computes base body wrench
            p_cm = obj.links{obj.baseLink}.cm_location;
            cm_wrench = [ obj.links{obj.baseLink}.base_force ; obj.links{obj.baseLink}.base_torque ];
            if ~isempty(obj.links{obj.baseLink}.sensor_frames) && ~isempty(varargin)
                sensor_id = varargin{1};
                if sensor_id ~= 0
                    R_sensor = R_rpy( obj.links{obj.baseLink}.sensor_frames{sensor_id}.ang );
                    p_sensor = obj.links{obj.baseLink}.sensor_frames{sensor_id}.pos;
                    g_CM_INS = [ R_sensor , (p_sensor - p_cm) ; 0 0 0 1 ];
                    base_wrench = (Ad( g_CM_INS )')*cm_wrench;
                else
                    g_CM_base = [ eye(3,3) , -p_cm ; 0 0 0 1 ];
                    base_wrench = (Ad( g_CM_base )')*cm_wrench;
%                     base_wrench = cm_wrench;
                end
            else
                g_CM_base = [ eye(3,3) , -p_cm ; 0 0 0 1 ];
                base_wrench = (Ad( g_CM_base )')*cm_wrench;
%                 base_wrench = cm_wrench;
            end
            
            % Computes joint torques
            tau_q = zeros(obj.numJoints,1);
            for i = 1:obj.numJoints
%                 tau_q(obj.joints{i}.id) = obj.joints{i}.tau_q;
                tau_q(i) = obj.joints{i}.tau_q;
            end
        end
        
        %% This function computes the direct dynamics algorithm of the whole tree.
        function varargout = DirectDynamics( obj, varargin )
        % tau_q = varargin{1}, tau_base = varargin{2}, unless a sensor is
        % defined at the base. If that is the case, then sensor_id =
        % varargin{1}, tau_q = varargin{2}, tau_base = varargin{3}.
        % ddq = varargout{1}, dV_0base = varargout{2}
            
            if isempty(obj.links{obj.baseLink}.sensor_frames)
                sensor_id = 0;
                switch length(varargin)
                    case 0
                        tau_q_in = obj.tau_q(:);
                        tau_base_in = obj.tau_base(:);
                    case 1
                        tau_q_in = varargin{1}(:);
                    case 2
                        tau_q_in = varargin{1}(:);
                        tau_base_in = varargin{2}(:);
                end
            else
                if isempty(varargin)
                    sensor_id = 0;
                else
                    sensor_id = varargin{1};
                    switch length(varargin)
                        case 2
                            tau_q_in = varargin{2}(:);
                        case 3
                            tau_q_in = varargin{2}(:);
                            tau_base_in = varargin{3}(:);
                        otherwise
                            tau_q_in = obj.tau_q(:);
                            tau_base_in = obj.tau_base(:);
                    end
                end
            end
            
            % Stores all tree configuration
            [ coord, twist_vel, twist_accel ] = obj.getBase( sensor_id );
            [ q_aux, dq_aux, ddq_aux ] = obj.getJoints();
            
            % Compute ddq
            Mqq = obj.Mqq();
            obj.setBase(sensor_id, coord, twist_vel, twist_accel);
            obj.setJoints( q_aux, dq_aux, zeros(obj.numJoints,1) );
            [ tau_q_line, ~ ] = obj.InverseDynamics(sensor_id,'extrinsic');
            varargout{1} = Mqq\(tau_q_in - tau_q_line);
            
            % Compute dV_0base
            if ~isempty(varargin) && (length(varargin) == 3)
                Mvv = obj.Mvv();
                obj.setBase( sensor_id, coord, twist_vel, zeros(6,1) );
                obj.setJoints( q_aux, dq_aux, ddq_aux );
                [ ~, tau_base_line ] = obj.InverseDynamics(sensor_id,'extrinsic');
                varargout{2} = Mvv\(tau_base_in - tau_base_line);
            end
            
            % Restores object original values
            obj.setBase( sensor_id, coord, twist_vel, twist_accel );
            obj.setJoints( q_aux, dq_aux, ddq_aux );
        end
        
        %% This function computes the regressor matrix of the whole tree using the Newton Euler algorithm.
        function [ Y, Pi ] = Regressor( obj )
            obj.DFS_Algorithm( 1, 1, 0, 1 );
            Y = obj.Y;
            Pi = obj.Pi(:);
        end
        
        %% This function computes the base link mass matrix using the Newton Euler algorithm.
        function Mvv = Mvv( obj, varargin )
        % If no sensor frame is defined, then varargin{1} means q (joint
        % variables). If one or more sensor frames are defined, varargin{1}
        % is its index and varargin{2} means q (joint variables).
        % If varargin is empty, Mvv is computed wrt the base CM.
            
            % Stores joint configuration
            [ q_aux, dq_aux, ddq_aux ] = obj.getJoints();
            
            if isempty(obj.links{obj.baseLink}.sensor_frames)
                sensor_id = 0;
                if isempty(varargin)
                    q_in = q_aux;
                else
                    q_in = varargin{1};
                end
            else
                if isempty(varargin)
                    sensor_id = 0;
                    q_in = q_aux;
                else
                    sensor_id = varargin{1};
                    q_in = varargin{2};
                end
            end
            
            % Stores base configuration
            [ coord, twist_vel, twist_accel ] = obj.getBase( sensor_id );
            
            % Main loop for computing the base mass matrix. 
            Mvv = zeros(6);
            for i = 1:6
                twist_accel_aux = zeros(6,1);
                twist_accel_aux(i) = 1;
                obj.setBase( sensor_id, zeros(6,1), zeros(6,1), twist_accel_aux );
                
%                 twist_accel_aux
%                 [ ~, ~, cm_linear_accel ] = obj.links{obj.baseLink}.bar2cm()
%                 obj.links{obj.baseLink}.ang_accel

                obj.setJoints( q_in, zeros(obj.numJoints,1), zeros(obj.numJoints,1) );
                
                % Calls 'intrinsic' so that no external forces are taken into account
                [ ~, Mvv_column ] = obj.InverseDynamics(sensor_id,'intrinsic');
                Mvv(:,i) = Mvv_column;
            end
            
            % Restores object original values
            obj.setBase( sensor_id, coord, twist_vel, twist_accel );
            obj.setJoints( q_aux, dq_aux, ddq_aux );
            
        end

        %% This function computes the base link mass matrix using the Newton Euler algorithm.
        function Mvq = Mvq( obj, varargin )
        % varargin means q (joint variables), since Mvv(q) is a function on
        % q only.
            
            % Stores joint configuration
            [ q_aux, dq_aux, ddq_aux ] = obj.getJoints();
            
            if isempty(obj.links{obj.baseLink}.sensor_frames)
                sensor_id = 0;
                if isempty(varargin)
                    q_in = q_aux;
                else
                    q_in = varargin{1};
                end
            else
                if isempty(varargin)
                    sensor_id = 0;
                    q_in = q_aux;
                else
                    sensor_id = varargin{1};
                    q_in = varargin{2};
                end
            end
            
            % Stores base configuration
            [ coord, twist_vel, twist_accel ] = obj.getBase( sensor_id );
            
            % Clear all base variables
            obj.setBase(sensor_id, coord, zeros(6,1), zeros(6,1));
            
            % Main loop for computing the joint mass matrix
            Mvq = zeros(6,obj.numJoints);
            for i = 1:obj.numJoints
                ddq_in = zeros(obj.numJoints,1);
%                 ddq_in(obj.joints{i}.id) = 1;
                ddq_in(i) = 1;
                obj.setJoints( q_in, zeros(obj.numJoints,1), ddq_in );
                
                % Calls 'intrinsic' so that no external forces are taken into account
                [ ~, Mvq_column ] = obj.InverseDynamics(sensor_id,'intrinsic');
                Mvq(:,i) = Mvq_column;
            end
            
            % Restores object original values
            obj.setBase( sensor_id, coord, twist_vel, twist_accel );
            obj.setJoints( q_aux, dq_aux, ddq_aux );
        end
        
        %% This function computes the crossed mass matrix using the Newton Euler algorithm.
        function Mqv = Mqv( obj, varargin )
        % varargin means q (joint variables), since Mqq(q) is a function on
        % q only.
        
            % Stores joint configuration
            [ q_aux, dq_aux, ddq_aux ] = obj.getJoints();
            
            if isempty(obj.links{obj.baseLink}.sensor_frames)
                sensor_id = 0;
                if isempty(varargin)
                    q_in = q_aux;
                else
                    q_in = varargin{1};
                end
            else
                if isempty(varargin)
                    sensor_id = 0;
                    q_in = q_aux;
                else
                    sensor_id = varargin{1};
                    q_in = varargin{2};
                end
            end
            
            % Stores base configuration
            [ coord, twist_vel, twist_accel ] = obj.getBase( sensor_id );

            % Main loop for computing the joint mass matrix
            Mqv = zeros(obj.numJoints, 6);
            for i = 1:6
                twist_accel_aux = zeros(6,1);
                twist_accel_aux(i) = 1;
                obj.setBase( sensor_id, zeros(6,1), zeros(6,1), twist_accel_aux );
%                 obj.setBase( sensor_id, coord, zeros(6,1), twist_accel_aux );
                obj.setJoints( q_in, zeros(obj.numJoints,1), zeros(obj.numJoints,1) );

                % Calls 'intrinsic' so that no external forces are taken into account
                [ Mqv_column, ~ ] = obj.InverseDynamics(sensor_id,'intrinsic');
                Mqv(:,i) = Mqv_column;
            end
            
            % Restores object original values
            obj.setBase( sensor_id, coord, twist_vel, twist_accel );
            obj.setJoints( q_aux, dq_aux, ddq_aux );
        end
        
        %% This function computes the joint mass matrix using the Newton Euler algorithm.
        function Mqq = Mqq( obj, varargin )
        % varargin means q (joint variables), since Mqq(q) is a function on
        % q only.
            
            % Stores joint configuration
            [ q_aux, dq_aux, ddq_aux ] = obj.getJoints();
            
            if isempty(obj.links{obj.baseLink}.sensor_frames)
                sensor_id = 0;
                if isempty(varargin)
                    q_in = q_aux;
                else
                    q_in = varargin{1};
                end
            else
                if isempty(varargin)
                    sensor_id = 0;
                    q_in = q_aux;
                else
                    sensor_id = varargin{1};
                    q_in = varargin{2};
                end
            end
            
            % Stores base configuration
            [ coord, twist_vel, twist_accel ] = obj.getBase( sensor_id );
            
            % Clear all base variables
            obj.setBase( sensor_id );
            
            % Main loop for computing the joint mass matrix
            Mqq = zeros(obj.numJoints);
            for i = 1:obj.numJoints
                ddq_in = zeros(obj.numJoints,1);
%                 ddq_in(obj.joints{i}.id) = 1;
                ddq_in(i) = 1;
                obj.setJoints( q_in, zeros(obj.numJoints,1), ddq_in );

                % Calls 'intrinsic' so that no external forces are taken into account
                [ Mqq_column, ~ ] = obj.InverseDynamics(sensor_id,'intrinsic');
                Mqq(:,i) = Mqq_column;
%                 Mqq(:,obj.joints{i}.id) = Mqq_column;
            end
            
            % Restores object original values
            obj.setBase( sensor_id, coord, twist_vel, twist_accel );
            obj.setJoints( q_aux, dq_aux, ddq_aux );
        end
        
        %% This function computes the vector of base centrifugal/Coriolis effects.
        function Cv_term = Cv( obj, varargin )
        % q = varargin{1}, dq = varargin{2}, base_config = varargin{3}, V_0b = varargin{4}
        % Cv_term = [ Cvv Cvq ]*[ V_0b ; dq ]
        
            % Stores joint configuration
            [ q_aux, dq_aux, ddq_aux ] = obj.getJoints();
            
            if isempty(obj.links{obj.baseLink}.sensor_frames)
                sensor_id = 0;
                [ coord_aux, twist_vel_aux, twist_accel_aux ] = obj.getBase( sensor_id );
                if isempty(varargin)
                    q_in = q_aux;
                    dq_in = dq_aux;
                    coord_in = coord_aux;
                    twist_vel_in = twist_vel_aux;
                else
                    q_in = varargin{1};
                    dq_in = varargin{2};
                    coord_in = varargin{3};
                    twist_vel_in = varargin{4};
                end
            else
                if isempty(varargin)
                    sensor_id = 0;
                    [ coord_aux, twist_vel_aux, twist_accel_aux ] = obj.getBase( sensor_id );
                    q_in = q_aux;
                    dq_in = dq_aux;
                    coord_in = coord_aux;
                    twist_vel_in = twist_vel_aux;
                else
                    sensor_id = varargin{1};
                    [ coord_aux, twist_vel_aux, twist_accel_aux ] = obj.getBase( sensor_id );
                    q_in = varargin{2};
                    dq_in = varargin{3};
                    coord_in = varargin{4};
                    twist_vel_in = varargin{5};
                end
            end
                        
            % Computes Cv_term by nullifying all other terms of equation
            obj.setBase( sensor_id, coord_in, twist_vel_in, zeros(6,1) );
            obj.setJoints( q_in, dq_in, zeros(obj.numJoints,1) );
            [ ~, Cv_term ] = obj.InverseDynamics(sensor_id,'intrinsic');
            
            % Restores object original values
            obj.setBase( sensor_id, coord_aux, twist_vel_aux, twist_accel_aux );
            obj.setJoints( q_aux, dq_aux, ddq_aux );
        end
        
        %% This function computes the vector of joint centrifugal/Coriolis effects.
        function Cq_term = Cq( obj, varargin )
        % q = varargin{1}, dq = varargin{2}, base_config = varargin{3}, V_0b = varargin{4}
        % % Cq_term = [ Cqv Cqq ]*[ V_0b ; dq ]
        
            % Stores all tree configuration
            [ q_aux, dq_aux, ddq_aux ] = obj.getJoints();
            
            if isempty(obj.links{obj.baseLink}.sensor_frames)
                sensor_id = 0;
                [ coord_aux, twist_vel_aux, twist_accel_aux ] = obj.getBase( sensor_id );
                if isempty(varargin)
                    q_in = q_aux;
                    dq_in = dq_aux;
                    coord_in = coord_aux;
                    twist_vel_in = twist_vel_aux;
                else
                    q_in = varargin{1};
                    dq_in = varargin{2};
                    coord_in = varargin{3};
                    twist_vel_in = varargin{4};
                end
            else
                if isempty(varargin)
                    sensor_id = 0;
                    [ coord_aux, twist_vel_aux, twist_accel_aux ] = obj.getBase( sensor_id );
                    q_in = q_aux;
                    dq_in = dq_aux;
                    coord_in = coord_aux;
                    twist_vel_in = twist_vel_aux;
                else
                    sensor_id = varargin{1};
                    [ coord_aux, twist_vel_aux, twist_accel_aux ] = obj.getBase( sensor_id );
                    q_in = varargin{2};
                    dq_in = varargin{3};
                    coord_in = varargin{4};
                    twist_vel_in = varargin{5};
                end
            end
                        
            % Computes Cq_term by nullifying all other terms of equation
            obj.setBase( sensor_id, coord_in, twist_vel_in, zeros(6,1) );
            obj.setJoints( q_in, dq_in, zeros(obj.numJoints,1) );
            [ Cq_term, ~ ] = obj.InverseDynamics(sensor_id,'intrinsic');
            
            % Restores object original values
            obj.setBase( sensor_id, coord_aux, twist_vel_aux, twist_accel_aux );
            obj.setJoints( q_aux, dq_aux, ddq_aux );
        end
        
        %% This function computes the joint gravity vector using the Newton Euler algorithm.
        function Nv = Nv( obj, varargin )
        % varargin{1} = q, varargin{2} = base_config
        
            % Stores all tree configuration
            [ q_aux, dq_aux, ddq_aux ] = obj.getJoints();
            
            if isempty(obj.links{obj.baseLink}.sensor_frames)
                sensor_id = 0;
                [ coord_aux, twist_vel_aux, twist_accel_aux ] = obj.getBase( sensor_id );
                if isempty(varargin)
                    q_in = q_aux;
                    coord_in = coord_aux;
                else
                    q_in = varargin{1};
                    coord_in = varargin{2};
                end
            else
                if isempty(varargin)
                    sensor_id = 0;
                    [ coord_aux, twist_vel_aux, twist_accel_aux ] = obj.getBase( sensor_id );
                    q_in = q_aux;
                    coord_in = coord_aux;
                else
                    sensor_id = varargin{1};
                    [ coord_aux, twist_vel_aux, twist_accel_aux ] = obj.getBase( sensor_id );
                    q_in = varargin{2};
                    coord_in = varargin{3};
                end
            end

            % Computes Nq by nullifying all other terms of equation
            obj.setBase( sensor_id, coord_in, zeros(6,1), zeros(6,1) );
            obj.setJoints( q_in, zeros(obj.numJoints,1), zeros(obj.numJoints,1) );
            [ ~, Nv ] = obj.InverseDynamics(sensor_id,'extrinsic');
            
            % Restores object original values
            obj.setBase( sensor_id, coord_aux, twist_vel_aux, twist_accel_aux );
            obj.setJoints( q_aux, dq_aux, ddq_aux );
        end
        
        %% This function computes the joint gravity vector using the Newton Euler algorithm.
        function Nq = Nq( obj, varargin )
        % varargin{1} = q, varargin{2} = base_config
        
            % Stores all tree configuration
            [ q_aux, dq_aux, ddq_aux ] = obj.getJoints();

            if isempty(obj.links{obj.baseLink}.sensor_frames)
                sensor_id = 0;
                [ coord_aux, twist_vel_aux, twist_accel_aux ] = obj.getBase( sensor_id );
                if isempty(varargin)
                    q_in = q_aux;
                    coord_in = coord_aux;
                else
                    q_in = varargin{1};
                    coord_in = varargin{2};
                end
            else
                if isempty(varargin)
                    sensor_id = 0;
                    [ coord_aux, twist_vel_aux, twist_accel_aux ] = obj.getBase( sensor_id );
                    q_in = q_aux;
                    coord_in = coord_aux;
                else
                    sensor_id = varargin{1};
                    [ coord_aux, twist_vel_aux, twist_accel_aux ] = obj.getBase( sensor_id );
                    q_in = varargin{2};
                    coord_in = varargin{3};
                end
            end
            
            % Computes Nq by nullifying all other terms of equation
            obj.setBase( sensor_id, coord_in, zeros(6,1), zeros(6,1) );
            obj.setJoints( q_in, zeros(obj.numJoints,1), zeros(obj.numJoints,1) );
            [ Nq, ~ ] = obj.InverseDynamics(sensor_id,'extrinsic');
            
            % Restores object original values
            obj.setBase( sensor_id, coord_aux, twist_vel_aux, twist_accel_aux );
            obj.setJoints( q_aux, dq_aux, ddq_aux );
        end
        
    end
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%