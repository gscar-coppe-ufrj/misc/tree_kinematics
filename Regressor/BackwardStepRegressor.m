%% Backpropagation of forces and moments ( Newton-Euler algorithm ) using 
% the iterative computation of the regressor matrix Y(q,dq,ddq).

%% Get the array of sucessors of u
sucessors = find(Pre == u);

%% Definition of the dynamic parameter vector
m_i = tree.links{u}.mass;
inertia = tree.links{u}.inertia;
r_i = tree.links{u}.cm_location;

Pi1 = m_i*[ 1 ; r_i(:) ];
Pi2 = inertia(:);
Pi3 = m_i*[ r_i(1)^2 ; r_i(2)^2 ; r_i(3)^2 ; r_i(1)*r_i(2) ; r_i(1)*r_i(3) ; r_i(2)*r_i(3) ];

tree.links{u}.Pi = [ Pi1 ; Pi2 ; Pi3 ];

%% Dynamic variables
R_ibar = R_rpy( tree.links{u}.ang );
w_i = tree.links{u}.ang_vel;
dw_i = tree.links{u}.ang_accel;
ddp_ibar = tree.links{u}.lin_accel;

%% If it is not at the end of the branch yet...
sum_A_tilde_up = zeros(3,16*tree.numLinks);
sum_Pi_tilde_up = zeros(3,16*tree.numLinks);
sum_rA_tilde_up = zeros(3,16*tree.numLinks);
is_end_of_a_branch = (length(tree.adjacencyList{u}) == 1) && (u ~= tree.baseLink);
if ~is_end_of_a_branch
    for i = 1:length(sucessors)
        
        joint_index = tree.adjacencyMatrix( u, sucessors(i) );
        A_tilde_up = tree.links{sucessors(i)}.A_tilde;
        Pi_tilde_up = tree.links{sucessors(i)}.Pi_tilde;
        
        %% Sums all contact forces and torques from the upper joints
        l_i = R_ibar*tree.links{u}.joint_locations(:,joint_index);
        if strcmpi(tree.links{u}.joints{joint_index}.type, 'p')
            q_i = tree.links{u}.joints{joint_index}.q;
        else
            q_i = 0;
        end
        k_i = q_i*R_ibar*tree.links{u}.joint_axis(:,joint_index);
        
        sum_A_tilde_up = sum_A_tilde_up + A_tilde_up;
        sum_Pi_tilde_up = sum_Pi_tilde_up + Pi_tilde_up;
        sum_rA_tilde_up = sum_rA_tilde_up + hat( l_i + k_i )*A_tilde_up;
    end
end

%% Define lever arm for current joint
if u ~= tree.baseLink
    down_joint_index = tree.adjacencyMatrix(u,Pre(u));
    l_u = R_ibar*tree.links{u}.joint_locations(:,down_joint_index);
else
    l_u = zeros(3,1);
end

%% Is there gravity?
if ~isempty(tree.G)
    G = tree.G;
else
    G = zeros(3,1);
end

%% Compute composite matrices
E = eye(9);
H_i = ( hat(dw_i) + hat(w_i)^2 )*R_ibar;
A_i = [ ddp_ibar-G , H_i ];
P_i = [ zeros(3,1), -hat(ddp_ibar - G)*R_ibar ];
% P_i = R_ibar*[ zeros(3,1), -hat((R_ibar')*(ddp_i - G)) ];
B_i = R_ibar*B( (R_ibar')*dw_i ) + hat(w_i)*R_ibar*B( (R_ibar')*w_i );
Psi_i = R_ibar*psi_fun( (R_ibar')*H_i )*[ E(:,1), E(:,5), E(:,9), E(:,2) + E(:,4), E(:,3) + E(:,7), E(:,6) + E(:,8) ];

%% Compute A_tilde and Pi_tilde matrices. Pi vector is also computed.
index = (u-1)*16 + 1;

A_partial = zeros(3,16*tree.numLinks);
A_partial(:, index:(index+3)) = A_i;
A_tilde = A_partial + sum_A_tilde_up;
tree.links{u}.A_tilde = A_tilde;

Pi_partial = zeros(3,16*tree.numLinks);
Pi_partial( :, index:(index+3) ) = P_i;
Pi_partial( :, (index+4):(index+9) ) = B_i;
Pi_partial( :, (index+10):(index+15) ) = Psi_i;
Pi_tilde = Pi_partial + sum_rA_tilde_up - hat(l_u)*A_tilde + sum_Pi_tilde_up;
tree.links{u}.Pi_tilde = Pi_tilde;

tree.Pi(index:index+15) = tree.links{u}.Pi;

%% Computes Regressor matrix
if u == tree.baseLink
%     tree.Y(1:3,:) = tree.links{u}.A_tilde;
%     tree.Y(4:6,:) = tree.links{u}.Pi_tilde;
    tree.Y(1:3,:) = (R_ibar')*tree.links{u}.A_tilde;
    tree.Y(4:6,:) = (R_ibar')*tree.links{u}.Pi_tilde;
else
    axis = R_ibar*tree.links{u}.joint_axis(:,down_joint_index);
    if u < tree.baseLink
        plus = u;
    else
        plus = u-1;
    end
    if strcmpi( tree.links{u}.joints{down_joint_index}.type, 'p' )
%         tree.Y(6+plus,:) = (axis')*A_tilde;
        tree.Y(6+plus,:) = (axis')*tree.links{u}.A_tilde;
    elseif strcmpi( tree.links{u}.joints{down_joint_index}.type, 'r' )
%         tree.Y(6+plus,:) = (axis')*Pi_tilde;
        tree.Y(6+plus,:) = (axis')*tree.links{u}.Pi_tilde;
    end
end

% %% Computes Regressor matrix
% if u == tree.baseLink
% %     tree.Y(1:3,:) = tree.links{u}.A_tilde;
% %     tree.Y(4:6,:) = tree.links{u}.Pi_tilde;
%     tree.Y(1:3,:) = (R_ibar')*tree.links{u}.A_tilde;
%     tree.Y(4:6,:) = (R_ibar')*tree.links{u}.Pi_tilde;
% else
%     axis = R_ibar*tree.links{u}.joint_axis(:,down_joint_index);
%     
%     for i = 1:length(tree.joints)
%         if tree.links{u}.joints{down_joint_index}.id == tree.joints{i}.id
%             break
%         end
%     end
% 
%     if strcmpi( tree.joints{i}.type, 'p' )
%         tree.Y(6+tree.joints{i}.id,:) = (axis')*A_tilde;
%     elseif strcmpi( tree.joints{i}.type, 'r' )
%         tree.Y(6+tree.joints{i}.id,:) = (axis')*Pi_tilde;
%     end
% end