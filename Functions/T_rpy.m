function T_rpy = T_rpy(eta2)
% T_rpy(eta2) computes the RPY velocity transformation matrix T(eta2)
phi=eta2(1);
theta=eta2(2);
T_rpy=[1 sin(phi)*tan(theta) cos(phi)*tan(theta); 0 cos(phi) -sin(phi); 0 sin(phi)/cos(theta) cos(phi)/cos(theta)];
end