function R_rpy = R_rpy(eta2)
%R_rpy(eta2) computes the RPY rotation matrix R=Rz(psi)Ry(theta)Rx(phi)
phi=eta2(1);
theta=eta2(2);
psi=eta2(3);

% R_rpy=Rz(psi)*Ry(theta)*Rx(phi);
R_rpy = [ cos(psi)*cos(theta), cos(psi)*sin(phi)*sin(theta) - cos(phi)*sin(psi), sin(phi)*sin(psi) + cos(phi)*cos(psi)*sin(theta) ;...
          cos(theta)*sin(psi), cos(phi)*cos(psi) + sin(phi)*sin(psi)*sin(theta), cos(phi)*sin(psi)*sin(theta) - cos(psi)*sin(phi) ;...
                  -sin(theta),                              cos(theta)*sin(phi),                              cos(phi)*cos(theta) ];

end