classdef Joint < handle
% This simple class creates objects of 'joint' class for RigidLink class.
% For now, they can be fixed, revolute or prismatic.
    
    properties
        name
        type
        id
        
        q
        dq
        ddq
        
        tau_q
        f_contact
        tau_contact
        
        offset
    end
    
    methods
        
        %% Class constructor
        function obj = Joint(varargin)
            switch nargin
                case 1
                    obj.name = varargin{1};
                case 2
                    obj.name = varargin{1};
                    obj.type = varargin{2};
                case 3
                    obj.name = varargin{1};
                    obj.type = varargin{2};
                    obj.id = varargin{3};
                otherwise
                    obj.name = '';
                    obj.offset = 0;
            end
        end
    end
    
end

