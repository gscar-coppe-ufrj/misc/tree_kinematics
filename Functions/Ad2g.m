function g = Ad2g( Ad )
% Returns homogeneous transformation equivalent to Adjoint matrix

R = Ad(1:3,1:3);
p = dehat(Ad(1:3,4:6)*(Ad(1:3,1:3).'));
g = [ R p ; 0 0 0 1 ];

end

