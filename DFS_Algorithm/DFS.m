function [ color, Pre ] = DFS( tree, FW_flag, BP_flag, FORCE_flag, RG_flag )
%% Depth-first search algorithm, adapted for Newton Euler.

% Root node
s = tree.baseLink;

% Number of states
N = length(tree.adjacencyList);

% Initialization step.
color = zeros(1,N); % Colors of each vertex: 0 -> white; 1 -> gray; 2 -> black
Pre = zeros(1,N); % Predecessors

[ color, Pre ] = DFS_Visit( tree, s, color, Pre, FW_flag, BP_flag, FORCE_flag, RG_flag );

end