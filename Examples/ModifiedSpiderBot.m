%% Test script for RigidLinkTree class.
clear all; clc

%% Define joints
offset1 = 0;
offset2 = 0;
offset3 = 0;
offset4 = 0;
offset5 = 0;

revolute_joint1 = Joint('revolute_joint1','r',offset1);
revolute_joint2 = Joint('revolute_joint2','r',offset2);
revolute_joint3 = Joint('revolute_joint3','r',offset3);
revolute_joint4 = Joint('revolute_joint4','r',offset4);
revolute_joint5 = Joint('revolute_joint5','r',offset5);

%% Define rigid bodies (links)
% Link C.M.s
r1 = 1; r2 = 1; r3 = 1;
rx1 = -r1*cos(pi/6); ry1 = r1*sin(pi/6);
rx2 = 0; ry2 = -r2;
rx3 = r1*cos(pi/6); ry3 = r1*sin(pi/6);
rx4 = 1; ry4 = 1;
rx5 = 1; ry5 = 1;

% Masses and inertias
Ixx0 = 1; Iyy0 = 1; Izz0 = 1;
Ixx1 = 1; Iyy1 = 1; Izz1 = 1;
Ixx2 = 1; Iyy2 = 1; Izz2 = 1;
Ixx3 = 1; Iyy3 = 1; Izz3 = 1;
Ixx4 = 1; Iyy4 = 1; Izz4 = 1;
Ixx5 = 1; Iyy5 = 1; Izz5 = 1;

m0 = 1; inertia0 = [Ixx0 Iyy0 Izz0 0 0 0]; cm_location0 = [0 0 0];
m1 = 1; inertia1 = [Ixx1 Iyy1 Izz1 0 0 0]; cm_location1 = [rx1 ry1 0];
m2 = 1; inertia2 = [Ixx2 Iyy2 Izz2 0 0 0]; cm_location2 = [rx2 ry2 0];
m3 = 1; inertia3 = [Ixx3 Iyy3 Izz3 0 0 0]; cm_location3 = [rx3 ry3 0];
m4 = 1; inertia4 = [Ixx4 Iyy4 Izz4 0 0 0]; cm_location4 = [rx4 ry4 0];
m5 = 1; inertia5 = [Ixx5 Iyy5 Izz5 0 0 0]; cm_location5 = [rx5 ry5 0];

body0 = RigidLink( 'body0', m0, inertia0, cm_location0 );
body1 = RigidLink( 'body1', m1, inertia1, cm_location1 );
body2 = RigidLink( 'body2', m2, inertia2, cm_location2 );
body3 = RigidLink( 'body3', m3, inertia3, cm_location3 );
body4 = RigidLink( 'body4', m4, inertia4, cm_location4 );
body5 = RigidLink( 'body5', m5, inertia5, cm_location5 );

%% Define joint axis and locations
axis = [0 0 1]; % planar XY

l0 = 1; l1 = 1; l2 = 1; l3 = 1;
joint1_location_wrt_body0 = [-l0*cos(pi/6),  l0*sin(pi/6), 0];
joint2_location_wrt_body0 = [      0      , -l0*sin(pi/6), 0];
joint3_location_wrt_body0 = [+l0*cos(pi/6),  l0*sin(pi/6), 0];

joint1_location_wrt_body1 = zeros(3,1);
joint2_location_wrt_body2 = zeros(3,1);
joint3_location_wrt_body3 = zeros(3,1);

joint4_location_wrt_body3 = [l3*cos(pi/6), l3*sin(pi/6), 0];
joint4_location_wrt_body4 = zeros(3,1);

joint5_location_wrt_body3 = [(l3/2)*cos(pi/6), (l3/2)*sin(pi/6), 0];
joint5_location_wrt_body5 = zeros(3,1);

body0.addJoint(revolute_joint1, axis, joint1_location_wrt_body0);
body0.addJoint(revolute_joint2, axis, joint2_location_wrt_body0);
body0.addJoint(revolute_joint3, axis, joint3_location_wrt_body0);

body1.addJoint(revolute_joint1, axis, joint1_location_wrt_body1);
body2.addJoint(revolute_joint2, axis, joint2_location_wrt_body2);
body3.addJoint(revolute_joint3, axis, joint3_location_wrt_body3);

body3.addJoint( revolute_joint4, axis, joint4_location_wrt_body3 );
body3.addJoint( revolute_joint5, axis, joint5_location_wrt_body3 );

body4.addJoint( revolute_joint4, axis, joint4_location_wrt_body4 );
body5.addJoint( revolute_joint5, axis, joint5_location_wrt_body5 );

%% Define robot
mech = RigidLinkTree('mech');
mech.addLink(body0);
mech.addLink(body1);
mech.addLink(body2);
mech.addLink(body3,'base');
mech.addLink(body4);
mech.addLink(body5);

%% Connect links
mech.connectLinks( 'body0', 'body1', 'revolute_joint1' );
mech.connectLinks( 'body0', 'body2', 'revolute_joint2' );
mech.connectLinks( 'body0', 'body3', 'revolute_joint3' );
mech.connectLinks( 'body3', 'body4', 'revolute_joint4' );
mech.connectLinks( 'body3', 'body5', 'revolute_joint5' );

%% Initialize baseLink variables
mech.links{mech.baseLink}.pos = [ 0 0 0 ]';
mech.links{mech.baseLink}.ang = [ 0 0 0 ]';
mech.links{mech.baseLink}.body_lin_vel = [ 0 0 0 ]';
mech.links{mech.baseLink}.body_ang_vel = [ 0 0 0 ]';
mech.links{mech.baseLink}.body_lin_accel = [ 0 0 0 ]';
mech.links{mech.baseLink}.body_ang_accel = [ 0 0 0 ]';

%% Initialize joint variables
mech.initTree();

%% Apply gravity force on the C.M.s
g = 9.81;
% mech.gravity(g);

