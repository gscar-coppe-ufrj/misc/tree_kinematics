function Bv = B( v )

Bv1 = blkdiag(v(1), v(2), v(3));
Bv2 = [ v(2), v(3), 0 ; v(1), 0, v(3) ; 0, v(1), v(2) ];
Bv = [ Bv1, Bv2 ];

end